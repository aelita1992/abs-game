﻿using UnityEngine;
using System.Collections;

public class SpawnerTrigger : MonoBehaviour {

	private Spawner spawner;
	void Start () {
		spawner = GetComponentInParent<Spawner> ();
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player" && spawner.isTriggered)
			spawner.spawn ();
	}
}
