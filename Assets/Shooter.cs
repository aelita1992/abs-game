﻿using UnityEngine;
using System.Collections;

public class Shooter : Enemy {
	
	public GameObject collisionOther;
	public float attackDistance;
	
	public virtual void Start()
	{
		base.Start ();

		
	}
	
	public virtual void Update()
	{
		base.Update ();

	}

	public virtual void shoot(int i)
	{
		Instantiate (bullet[i], shotSpawn[i].transform.position, shotSpawn[i].transform.rotation);
		//obj.GetComponent <ShooterBullet>().setSpeedAndDamage(bulletSpeed[i], damage[i]);
	}
}