﻿using UnityEngine;
using System.Collections;

public class EnemyGroupController : MonoBehaviour {

    private Enemy[] children;
	// Use this for initialization
	void Start () {
        loadChildrenArray();

        //Debug.Log(children.Length);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void loadChildrenArray()
    {
        children = GetComponentsInChildren<Enemy>();
    }

    public void broadcastHit()
    {
        Debug.Log("DUPADUPA");
        for(int i=0; i<children.Length; i++)
        {
            NavMeshAgent agent = children[i].GetComponent<NavMeshAgent>();
            if (children[i].isAlive && agent.enabled)
            {
                agent.SetDestination(Game.player.transform.position);
                agent.Resume();
            }
        }

    }
}
