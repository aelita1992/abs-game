﻿public class PlayerAnimProperty
{
	public const string IsIdle 				= "IsIdle";
	public const string IsWalking			= "IsWalking";
	public const string IsWalkingForward	= "IsWalkingForward";
	public const string IsWalkingBackwards	= "IsWalkingBackwards";
	public const string IsTurning 			= "IsTurning";
	public const string IsRolling 			= "IsRolling";
	public const string IsKicking 			= "IsKicking";
	public const string IsUsingTerminal 	= "IsUsingTerminal";
	public const string IsCrouching 		= "IsCrouching";
	public const string IsTakingWeapon 		= "IsTakingWeapon";
	public const string IsHit 				= "IsHit";
	public const string IsHitFromBack 		= "IsHitFromBack";
	public const string IsShooting 			= "IsShooting";
	public const string IsDead 				= "IsIdle";
	public const string FlightEnabled 		= "FlightEnabled";
	public const string IsFlyingLeft 		= "IsFlyingLeft";
	public const string IsFlyingRight 		= "IsFlyingRight";
	public const string IsFlyingForward 	= "IsFlyingForward";
	public const string IsFlyingBackwards 	= "IsFlyingBackwards";
	public const string IsFlyingIdle 		= "IsFlyingIdle";

	public const string Float_RollState 	= "RollState";
	public const string Float_PlayerSpeed 	= "PlayerSpeed";
}