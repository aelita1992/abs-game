﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : Pickup 
{
	public AmmoEnum ammoType;
	public int 		quantity;
	
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Player")
		{
			WeaponManager.Get ().AddAmmo(ammoType, quantity);
			gameObject.SetActive (false);
		}		
	}
}
