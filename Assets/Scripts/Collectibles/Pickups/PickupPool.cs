﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickupPool : MonoBehaviour
{
	private static PickupPool singleton;
	public static PickupPool Get()
	{
		return singleton;
	}

	int pickupIndex = 0;
	private List <GameObject> pickupPool;
	private List <GameObject> prefabs;

	public GameObject hpPickupPrefab;
	public GameObject shieldPickupPrefab;
	public GameObject energyPickupPrefab;

	public GameObject bulletPickupPrefab;
	public GameObject rocketPickupPrefab;
	public GameObject grenadePickupPrefab;

	void Awake()
	{
		singleton = this;

		prefabs = new List<GameObject>();
		pickupPool = new List<GameObject>();

		CreatePrefabsList();
		PopulatePool ();

		Reshuffle(pickupPool);
	}

	void CreatePrefabsList()
	{
		prefabs.Add (hpPickupPrefab);
		prefabs.Add (shieldPickupPrefab);
		prefabs.Add (energyPickupPrefab);

		prefabs.Add (bulletPickupPrefab);
		prefabs.Add (rocketPickupPrefab);
		prefabs.Add (grenadePickupPrefab);
	}

	void PopulatePool()
	{
		GameObject temp;

		for(int i = 0; i < prefabs.Count; i++)
		{
			temp = Instantiate (prefabs[i]);
			temp.transform.parent = this.gameObject.transform;

			pickupPool.Add (temp);
			temp.gameObject.SetActive (false);
		}
	}
	
	void Reshuffle(List <GameObject> pickups)
	{
		for (int i = 0; i < pickups.Count; i++ )
		{
			GameObject tmp = pickups[i];
			int randomInd = Random.Range(i, pickups.Count);
			pickups[i] = pickups[randomInd];
			pickups[randomInd] = tmp;
		}
	}

	public GameObject GetRandomPickup()
	{
		GameObject pickup = pickupPool[pickupIndex];
		pickupIndex++;

		if(pickupIndex >= pickupPool.Count)
		{
			Reshuffle(pickupPool);
			pickupIndex = 0;
		}

		return pickup;
	}
}
