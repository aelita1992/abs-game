﻿using UnityEngine;
using System.Collections;

public class StatPickup : Pickup 
{
	public StatEnum statistic;
	public int 		quantity;

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Player")
		{
			other.gameObject.GetComponent<Player>().ModifyStat (statistic, quantity);
			gameObject.SetActive (false);
		}		
	}
}
