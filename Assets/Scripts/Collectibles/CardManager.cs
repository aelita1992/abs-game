﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardManager 
{
	private static CardManager singleton;
	public static CardManager Get()
	{
		if(singleton == null)
			singleton = new CardManager();

		return singleton;
	}

	private Dictionary <CardEnum, bool> cards;

	private CardManager()
	{
		cards = new Dictionary<CardEnum, bool>();

		cards.Add (CardEnum.BLACK_CARD,		false);
		cards.Add (CardEnum.BLUE_CARD,		false);
		cards.Add (CardEnum.GREEN_CARD, 	false);
		cards.Add (CardEnum.RED_CARD, 		false);
		cards.Add (CardEnum.YELLOW_CARD,	false);
	}

	public void AddCard(CardEnum cardType)
	{
		cards[cardType] = true;
	}

	public bool IsCardCollected(CardEnum cardType)
	{
		return cards[cardType];
	}

}
