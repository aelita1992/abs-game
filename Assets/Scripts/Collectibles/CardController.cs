﻿using UnityEngine;
using System.Collections;

public class CardController : MonoBehaviour 
{
	public CardEnum cardType;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;

		CardManager.Get ().AddCard (cardType);
		Destroy (this.gameObject);
	}
}
