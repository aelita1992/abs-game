﻿using UnityEngine;
using System.Collections;

public class WeaponCollectible : MonoBehaviour 
{
	public int 			weaponType;
	public GameObject	weaponModel;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;

		HelpTextController.Get ().Enable();
		HelpTextController.Get ().SetText("New weapon unlocked, press " + (weaponType + 1) + " to activate.");
		WeaponManager.Get ().UnlockWeapon (weaponType);

		TimeManager.Get ().timeDependantUpdate += AwaitWeaponTest;
		GetComponent<Collider>().enabled = false;
		Destroy (weaponModel);
	}

	void AwaitWeaponTest()
	{
		int weaponInput = GetWeaponInputValue();

		if(weaponInput == weaponType + 1)
		{
			TimeManager.Get ().timeDependantUpdate -= AwaitWeaponTest;
			HelpTextController.Get ().Disable();
		}
	}

	private int GetWeaponInputValue()
	{
		if (Input.GetButtonDown ("Weapon1")) return 1;
		if (Input.GetButtonDown ("Weapon2")) return 2;
		if (Input.GetButtonDown ("Weapon3")) return 3;
		if (Input.GetButtonDown ("Weapon4")) return 4;
		if (Input.GetButtonDown ("Weapon5")) return 5;
		if (Input.GetButtonDown ("Weapon6")) return 6;

		return -1;
	}
}
