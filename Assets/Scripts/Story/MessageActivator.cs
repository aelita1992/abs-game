﻿using UnityEngine;
using System.Collections;

public class MessageActivator : MonoBehaviour
{
	public MessageID messageID;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;
	
		MessageEntry entry = MessagesLibrary.Get ().GetMessageEntry(messageID);

		if(entry != null)
			HUD_Controller.Get ().messageBoxController.EnableMessageBox(entry.avatarID, entry.title, entry.message);

		Destroy (this);
	}
}
