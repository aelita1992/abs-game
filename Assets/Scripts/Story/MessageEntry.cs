﻿using UnityEngine;
using System.Collections;

public class MessageEntry
{
	public AvatarEnum avatarID;
	
	public string title;
	public string message;
	
	public MessageEntry(AvatarEnum _avatarID, string _title, string _message)
	{
		avatarID = _avatarID;
		title = _title;
		message = _message;
	}
}