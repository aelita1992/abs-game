﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MessageID
{
	STORY1,
	STORY2
}

public class MessagesLibrary
{
	private static MessagesLibrary singleton;
	public static MessagesLibrary Get()
	{
		if(singleton == null)
			singleton = new MessagesLibrary();

		return singleton;
	}

	private Dictionary <MessageID, MessageEntry> library;

	private MessagesLibrary()
	{
		library = new Dictionary<MessageID, MessageEntry>();

		library.Add (MessageID.STORY1, new MessageEntry(AvatarEnum.GUIDE, "Greetings", "Musisz wydostać się z tego poziomu\nBędzie ciężko, ale musisz spróbować.\nMusisz iść na północ."));
	}

	public MessageEntry GetMessageEntry(MessageID id)
	{
		return library[id];
	}
}
