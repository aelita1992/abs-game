﻿using UnityEngine;
using System.Collections;

public class TextFieldsController : MonoBehaviour 
{
	public TextMesh titleField;
	public TextMesh messageField;
	public TextMesh progressField;

	public void EnableTextFields()
	{
		gameObject.SetActive (true);
	}
	
	public void DisableTextFields()
	{
		gameObject.SetActive (false);
	}

	public void SetBoxContents(string title, string message)
	{
		titleField.text = title;
		messageField.text = message;
	}

	public void SetProgressText(string newText)
	{
		progressField.text = newText;
	}
}
