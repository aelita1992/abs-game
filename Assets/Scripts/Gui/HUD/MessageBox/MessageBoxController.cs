﻿using UnityEngine;
using System.Collections;

public class MessageBoxController : MonoBehaviour 
{
	public TextFieldsController textController;
	public AvatarsManager avatarsManager;
	public GameObject bordersObject;

	public Animator legAnimator;
	public Animator upBodyAnimator;

	public void EnableMessageBox(AvatarEnum avatarID ,string title, string message)
	{
		textController.EnableTextFields();
		textController.SetBoxContents(title, message);
		avatarsManager.ShowAvatar(avatarID);

		TimeManager.Get ().PauseGame();
		Game.player.PauseRigidbody();
		legAnimator.speed = 0;
		upBodyAnimator.speed = 0;

		TimeManager.Get ().nonPausableUpdate += MessageBoxInputControl;
		bordersObject.SetActive (true);
	}

	public void DisableMessageBox()
	{
		textController.DisableTextFields();
		avatarsManager.HideCurrentAvatar();

		TimeManager.Get ().ResumeGame();
		Game.player.ResumeRigidbody();
		legAnimator.speed = 1;
		upBodyAnimator.speed = 1;

		TimeManager.Get ().nonPausableUpdate -= MessageBoxInputControl;
		bordersObject.SetActive (false);
	}

	void MessageBoxInputControl()
	{
		if(Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.Space))
			DisableMessageBox();
	}

}
