﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvatarsManager : MonoBehaviour 
{
	public AvatarController guideController;
	public AvatarController aiController;

	private AvatarController activeAvatar;

	private Dictionary <AvatarEnum, AvatarController> avatars;

	void Init()
	{
		avatars = new Dictionary <AvatarEnum, AvatarController> ();

		avatars.Add (AvatarEnum.GUIDE, guideController);
		avatars.Add (AvatarEnum.AI, aiController);
	}

	public void ShowAvatar(AvatarEnum avatarID)
	{
		if(avatars == null)
			Init ();

		this.gameObject.SetActive(true);

		if(activeAvatar != null)
			activeAvatar.HideAvatar();

		activeAvatar = avatars[avatarID];
		Invoke ("ShowActiveAvatar", 0.1f);
	}

	public void HideCurrentAvatar()
	{
		if(activeAvatar != null)
			activeAvatar.HideAvatar();
	}

	void ShowActiveAvatar()
	{
		activeAvatar.ShowAvatar();
	}


}
