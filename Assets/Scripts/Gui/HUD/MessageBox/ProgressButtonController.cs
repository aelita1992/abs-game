﻿using UnityEngine;
using System.Collections;

public class ProgressButtonController : MonoBehaviour
{
	public TextMesh progressTextMesh;
	public MessageBoxController messBoxController;

	private Color defaultcolor;
	public Color mouseOverColor;

	void Awake()
	{
		defaultcolor = progressTextMesh.color;
	}

	void OnMouseEnter()
	{
		progressTextMesh.color = mouseOverColor;
	}

	void OnMouseDown()
	{
		messBoxController.DisableMessageBox();
	}

	void OnMouseExit()
	{
		progressTextMesh.color = defaultcolor;
	}
}
