﻿using UnityEngine;
using System.Collections;

public class AvatarController : MonoBehaviour
{
	private Animator avatarAnimator;
	
	void Awake()
	{
		avatarAnimator = GetComponent<Animator>();
	}
	
	public void ShowAvatar()
	{
		avatarAnimator.SetBool("IsShowing", true);
	}
	
	public void HideAvatar()
	{
		avatarAnimator.SetBool("IsHiding", true);
	}
	
	void DisableIsShowing()
	{
		avatarAnimator.SetBool("IsShowing", false);
	}
	
	void DisableIsHiding()
	{
		avatarAnimator.SetBool("IsHiding", false);
	}
}
