﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatBarsController : MonoBehaviour 
{
	public class StatFieldHolder
	{
		StatField[] connectedFields;
		Vector2 currAndTargetStat;

		public void SetTargetValue(int targetVal)
		{
			currAndTargetStat.y = targetVal;
		}

		public void ActivateField()
		{
			if(currAndTargetStat.x >= connectedFields.Length)
				return;

			connectedFields[(int)currAndTargetStat.x].ShowField();
			currAndTargetStat.x++;

			if(currAndTargetStat.x >= currAndTargetStat.y)
				OnUpdate -= ActivateField;
		}

		public void DeactivateField()
		{
			currAndTargetStat.x--;
			connectedFields[(int)currAndTargetStat.x].HideField();
			
			if(currAndTargetStat.x <= currAndTargetStat.y)
				OnUpdate -= DeactivateField;
		}

		public int GetDirection()
		{
			if(currAndTargetStat.y - currAndTargetStat.x == 0)
				return 0;

			if(currAndTargetStat.x > currAndTargetStat.y)
				return -1;

			return 1;
		}

		public StatFieldHolder(StatField[] connFields)
		{
			connectedFields = connFields;
			currAndTargetStat = new Vector2(0, 10);
		}
	};

	public delegate void StatBarUpdate();
	public static event StatBarUpdate OnUpdate;
	
	private Dictionary<StatEnum, StatFieldHolder> statFieldHolders;

	public StatField[] healthFields;
	public StatField[] shieldFields;
	public StatField[] energyFields;

	float fieldChangeTimer = 0.2f;
	float fieldChangeCooldown = 0.2f;

	void Awake()
	{
		statFieldHolders = new Dictionary<StatEnum, StatFieldHolder>();

		statFieldHolders.Add (StatEnum.HEALTH, new StatFieldHolder(healthFields));
		statFieldHolders.Add (StatEnum.SHIELD, new StatFieldHolder(shieldFields));
		statFieldHolders.Add (StatEnum.ENERGY, new StatFieldHolder(energyFields));

		SetTargetValue(StatEnum.HEALTH, 10);
		SetTargetValue(StatEnum.SHIELD, 10);
		SetTargetValue(StatEnum.ENERGY, 10);
	}

	void OnEnable()
	{
		TimeManager.Get ().timeDependantUpdate += CustomUpdate;
	}

	void OnDisable()
	{
		TimeManager.Get ().timeDependantUpdate -= CustomUpdate;
	}

	public void SetTargetValue(StatEnum statType, int targetValue)
	{
		OnUpdate -= statFieldHolders[statType].DeactivateField;
		OnUpdate -= statFieldHolders[statType].ActivateField;

		statFieldHolders[statType].SetTargetValue(targetValue);

		int direction = statFieldHolders[statType].GetDirection();

		if(direction > 0)
		{
			OnUpdate += statFieldHolders[statType].ActivateField;
		}

		if(direction < 0)
		{
			OnUpdate += statFieldHolders[statType].DeactivateField;
		}
	}

	void CustomUpdate()
	{
		if(fieldChangeTimer > 0)
		{
			fieldChangeTimer -= Time.deltaTime;
		}
		else
		{
			if(OnUpdate != null)
				OnUpdate();

			fieldChangeTimer = fieldChangeCooldown;
		}
	}

}
