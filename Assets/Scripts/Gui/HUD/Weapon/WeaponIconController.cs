﻿using UnityEngine;
using System.Collections;

public class WeaponIconController : MonoBehaviour
{
	public WeaponIcon[] weaponIcons;
	WeaponIcon currentWeapon;
	int currWeaponIndex;

	void Awake()
	{
		currentWeapon = weaponIcons[0];
		currWeaponIndex = 0;
	}

	public void ActivateWeaponField(int fieldIndex)
	{
		if(fieldIndex == currWeaponIndex)
			return;

		if(fieldIndex >= 0 && fieldIndex < weaponIcons.Length)
		{
			currentWeapon.HideField();
			currentWeapon = weaponIcons[fieldIndex];
			currWeaponIndex = fieldIndex;

			Invoke ("ShowCurrentWeapon", 0.1f);
		}
	}

	void ShowCurrentWeapon()
	{
		currentWeapon.ShowField();
	}
}
