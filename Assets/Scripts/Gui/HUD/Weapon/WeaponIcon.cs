﻿using UnityEngine;
using System.Collections;

public class WeaponIcon : MonoBehaviour 
{
	private Animator fieldAnimator;
	
	void Awake()
	{
		fieldAnimator = GetComponent<Animator>();
	}
	
	public void ShowField()
	{
		fieldAnimator.SetBool("IsShowing", true);
	}
	
	public void HideField()
	{
		fieldAnimator.SetBool("IsHiding", true);
	}
	
	void DisableIsShowing()
	{
		fieldAnimator.SetBool("IsShowing", false);
	}
	
	void DisableIsHiding()
	{
		fieldAnimator.SetBool("IsHiding", false);
	}
}
