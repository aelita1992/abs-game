﻿using UnityEngine;
using System.Collections;

public class HUD_Controller : MonoBehaviour 
{
	private static HUD_Controller singleton;
	public static HUD_Controller Get()
	{
		return singleton;
	}

	private HUD_Controller(){		}

	public AmmoDisplay ammoDisplay;
	public StatBarsController statBarsController;
	public WeaponIconController weaponIconController;
	public MessageBoxController messageBoxController;

	void Awake()
	{
		singleton = this;
	}

	public AmmoDisplay GetAmmoDisplay()
	{
		return ammoDisplay;
	}

	public StatBarsController GetStatBarsController()
	{
		return statBarsController;
	}

	public WeaponIconController GetWaponIconController()
	{
		return weaponIconController;
	}

	public MessageBoxController GetMessageBoxController()
	{
		return messageBoxController;
	}
}
