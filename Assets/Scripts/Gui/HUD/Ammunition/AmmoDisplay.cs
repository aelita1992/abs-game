﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoDisplay : MonoBehaviour
{
	public TextMesh[] ammoTextMeshes;
	private Dictionary<AmmoEnum, TextMesh> textMeshes;

	void Awake()
	{
		textMeshes = new Dictionary<AmmoEnum, TextMesh>();

		textMeshes.Add (AmmoEnum.BULLET, ammoTextMeshes[0]);
		textMeshes.Add (AmmoEnum.ROCKET, ammoTextMeshes[1]);
		textMeshes.Add (AmmoEnum.GRENADE, ammoTextMeshes[2]);
	}

	public void SetDisplayedValue(AmmoEnum ammoType, int newValue)
	{
		textMeshes[ammoType].text = newValue.ToString();
	}
}
