﻿using UnityEngine;
using System.Collections;

public class Barrel_GroundFireRange : MonoBehaviour 
{
	bool playerInRange;

	void OnEnable()
	{
		playerInRange = false;
	}

	void OnTriggerEnter(Collider col) 
	{
		playerInRange = true;
	}

	void OnTriggerExit(Collider col) 
	{
		playerInRange = false;
	}

	public bool IsPlayerInRange()
	{
		return playerInRange;
	}
}
