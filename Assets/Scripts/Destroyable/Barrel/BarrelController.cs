﻿using UnityEngine;
using System.Collections;

public class BarrelController : MonoBehaviour
{
	public delegate void BarrelAction();
	public event BarrelAction OnHit;

	public GameObject barrelFlame;
	public GameObject barrelExplosion;
	public GameObject groundFire;

	public Barrel_ExplosionRange explRngController;
	public Barrel_GroundFireRange fireRngController;

	float enflameTimer;
	float fireDmgTimer;

	void OnEnable()
	{
		OnHit += EnflameBarrel;
		enflameTimer = 4f;
	}

	void OnDisable()
	{
		OnHit -= EnflameBarrel;
		OnHit -= DetonateBarrel;
	}

	void OnTriggerEnter(Collider col)
	{
		if(OnHit != null)
			OnHit();
	}

	void EnflameBarrel()
	{
		barrelFlame.SetActive (true);

		OnHit -= EnflameBarrel;
		OnHit += DetonateBarrel;
		TimeManager.Get ().timeDependantUpdate += FlameProgression;
	}

	void FlameProgression()
	{
		enflameTimer -= Time.deltaTime;

		if(enflameTimer <= 0)
		{
			OnHit();
			TimeManager.Get ().timeDependantUpdate -= FlameProgression;
		}
	}

	void DetonateBarrel()
	{
		TimeManager.Get ().timeDependantUpdate -= FlameProgression;

		barrelFlame.SetActive (false);
		barrelExplosion.SetActive (true);
		groundFire.SetActive (true);

		GetComponent<MeshRenderer>().enabled = false;
		GetComponent<CapsuleCollider>().enabled = false;

		if(explRngController.IsPlayerInRange())
			Game.player.hit (20);

		TimeManager.Get ().timeDependantUpdate += TestForFireDmg;
		fireDmgTimer = 1f;

		Invoke ("DisableBarrel", 12f);
		OnHit -= DetonateBarrel;
	}

	void TestForFireDmg()
	{
		if(fireDmgTimer > 0)
			fireDmgTimer -= Time.deltaTime;

		if(fireDmgTimer <= 0 && fireRngController.IsPlayerInRange())
		{
			fireDmgTimer = 1f;
			Game.player.hit (20);
		}
	}

	void DisableBarrel()
	{
		TimeManager.Get ().timeDependantUpdate -= TestForFireDmg;
		gameObject.SetActive (false);
	}
	
}
