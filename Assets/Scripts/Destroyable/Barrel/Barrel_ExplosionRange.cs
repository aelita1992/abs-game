﻿using UnityEngine;
using System.Collections;

public class Barrel_ExplosionRange : MonoBehaviour
{
	bool playerInRange;

	void OnEnable()
	{
		playerInRange = false;
	}

	void OnTriggerEnter(Collider col) 
	{
		playerInRange = true;
	}
	
	void OnTriggerExit(Collider col) 
	{
		playerInRange = false;
	}

	public bool IsPlayerInRange()
	{
		return playerInRange;
	}
}
