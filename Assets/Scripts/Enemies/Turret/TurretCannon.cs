﻿using UnityEngine;
using System.Collections;

public class TurretCannon : MonoBehaviour 
{
	delegate void TurretDelegate();
	event TurretDelegate TurretAction;

	Vector3 firingDirection;
	Vector3 targetPosition;

	float angleToRotate;

	float turningSpeed = 0.5f;
	float turningDirSign;

	float firingTimer;
	float nextShotTime;

	public void FireAtPosition(Vector3 targetPos)
	{
		targetPos.y = this.transform.position.y;
		targetPosition = targetPos;

		angleToRotate = Vector3.Angle (transform.forward, targetPos - transform.position);
		float directionTestAngle = Vector3.Angle (transform.right, targetPos - transform.position);
		float dirFromleft = Vector3.Angle (transform.right, targetPos - transform.position);
		float dirFromRight = Vector3.Angle (transform.right + new Vector3(0, 180, 0), targetPos - transform.position);
		
		if(dirFromleft >= dirFromRight)
			turningDirSign = -1;
		else
			turningDirSign = 1;

		nextShotTime = 0.5f;
		firingTimer = 5f;
		TurretAction += TurnToTarget;
	}

	void TurnToTarget()
	{
		transform.localEulerAngles += new Vector3(0, turningDirSign * turningSpeed, 0);
		angleToRotate -= Mathf.Abs(turningDirSign * turningSpeed);
		
		if(angleToRotate < 1)
		{
			transform.LookAt (targetPosition);
			TurretAction -= TurnToTarget;
			TurretAction += Firing;
		}
	}

	void Firing()
	{
		firingTimer -= Time.deltaTime;
		nextShotTime -= Time.deltaTime;

		if(nextShotTime <= 0)
		{
			GameObject bullet = TurretAmmoPool.Get().GetBullet();
			bullet.transform.position = this.transform.position + transform.forward;
			bullet.transform.eulerAngles = transform.rotation.eulerAngles;

			float spreadParameter = Random.Range(-10f, 10f);
			bullet.transform.eulerAngles += new Vector3(0, spreadParameter, 0);

			bullet.SetActive(true);
			nextShotTime = 0.1f;
		}

		if(firingTimer < 0)
			TurretAction -= Firing;
	}

	void FixedUpdate()
	{
		if(TurretAction != null)
			TurretAction();
	}
}
