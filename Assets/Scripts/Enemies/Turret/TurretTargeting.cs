﻿using UnityEngine;
using System.Collections;

public class TurretTargeting : MonoBehaviour
{
	delegate void TurretDelegate();
	static event TurretDelegate TurretAction;

	public TurretController turretController;
	public LayerMask targetsMask;
	public LineRenderer laserRenderer;

	public float laserRange;
	public Vector2 angleBoundaries;

	float currentAngle;
	float rotationSpeed;

	void Start () 
	{
		laserRenderer.SetPosition(1, new Vector3(0, 0, laserRange));
		rotationSpeed = 0.1f;
		TurretAction += Targeting;
	}

	void FixedUpdate ()
	{
		if(TurretAction != null)
			TurretAction();
	}

	void Targeting()
	{
		RotateTurret();

		RaycastHit hit;
		if(Physics.Raycast(transform.position, transform.forward, out hit, laserRange, targetsMask))
		{
			laserRenderer.SetPosition(1, new Vector3(0, 0, hit.distance));

			TurretAction -= Targeting;
			Vector3 targetCenter = hit.collider.transform.position;
			turretController.OpenFire(targetCenter);
		}
		else
		{
			laserRenderer.SetPosition(1, new Vector3(0, 0, laserRange));
		}
	}

	void RotateTurret()
	{
		transform.Rotate (new Vector3(0, rotationSpeed, 0));
		currentAngle += rotationSpeed;

		if(currentAngle > angleBoundaries.y)
			rotationSpeed = -0.1f;

		if(currentAngle < angleBoundaries.x)
			rotationSpeed = 0.1f;
	}
}
