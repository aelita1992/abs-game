﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour
{


	public GameObject[] missileLaunchers;
	public GameObject[] cannons;

	public void Awake()
	{
		foreach(GameObject launcher in missileLaunchers)
		{
			//launcher.GetComponent<Animator>().SetBool("activationTriggered", true);
		}

		foreach(GameObject cannon in cannons)
		{
			//cannon.GetComponent<Animator>().SetBool("activationTriggered", true);
		}
	}

	public void OpenFire(Vector3 targetPosition)
	{
		foreach(GameObject launcher in missileLaunchers)
		{
			launcher.GetComponent<TurretMissileLauncher>().FireAtPosition(targetPosition);
		}
		
		foreach(GameObject cannon in cannons)
		{
			cannon.GetComponent<TurretCannon>().FireAtPosition(targetPosition);
		}
	}
}
