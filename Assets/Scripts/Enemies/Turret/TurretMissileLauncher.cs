﻿using UnityEngine;
using System.Collections;

public class TurretMissileLauncher : MonoBehaviour 
{
	delegate void TurretDelegate();
	event TurretDelegate TurretAction;
	
	Vector3 firingDirection;
	Vector3 targetPosition;

	float angleToRotate;
	float turningSpeed = 0.5f;
	float turningDirSign;
	float nextShotTime;

	int missileIterator = 0;
	public TurretMissile[] missiles;

	void OnEnable()
	{
		missileIterator = 0;
		ShuffleMissiles();
	}
	
	void ShuffleMissiles ()
	{
		for (int i = 0; i < missiles.Length; i++) 
		{
			TurretMissile temp = missiles[i];
			int randomIndex = Random.Range(0, missiles.Length);
			missiles[i] = missiles[randomIndex];
			missiles[randomIndex] = temp;
		}
	}

	public void FireAtPosition(Vector3 targetPos)
	{
		targetPos.y = this.transform.position.y;
		targetPosition = targetPos;

		angleToRotate = Vector3.Angle (transform.forward, targetPos - transform.position);
		float directionTestAngle = Vector3.Angle (transform.right, targetPos - transform.position);
		float dirFromleft = Vector3.Angle (transform.right, targetPos - transform.position);
		float dirFromRight = Vector3.Angle (transform.right + new Vector3(0, 180, 0), targetPos - transform.position);

		if(dirFromleft >= dirFromRight)
			turningDirSign = -1;
		else
			turningDirSign = 1;


		nextShotTime = 1f;
		TurretAction += TurnToTarget;
	}

	void TurnToTarget()
	{
		transform.localEulerAngles += new Vector3(0, turningDirSign * turningSpeed, 0);
		angleToRotate -= Mathf.Abs(turningDirSign * turningSpeed);

		if(angleToRotate < 1)
		{
			transform.LookAt (targetPosition);
			TurretAction -= TurnToTarget;
			TurretAction += Firing;
		}
	}
	
	void Firing()
	{
		nextShotTime -= Time.deltaTime;
		
		if(nextShotTime <= 0)
		{
			missiles[missileIterator].ActivateMissile();
			missileIterator++;

			nextShotTime = 0.15f;
		}

		if(missileIterator >= missiles.Length)
			TurretAction -= Firing;
	}
	
	void FixedUpdate()
	{
		if(TurretAction != null)
			TurretAction();
	}
}
