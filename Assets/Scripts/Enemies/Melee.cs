using UnityEngine;
using System.Collections;

public class Melee : Enemy {
	
	public float dashForce;
	public float dashFriction;
	private Vector3 dashVelocity;
	public float dashCooldown;
	public float dashDistance;
	private bool isDashing;
	public int damage;

	public GameObject collisionOther;

	public override void Start()
	{
		base.Start ();
		dashVelocity = new Vector3 ();
		isDashing = false;
	}

    public override void die()
    {
        if (isAlive)
        {
            //jakies partikle tu i inne rzeczy
            isAlive = false;
            //int rand = Random.Range(0, blow.Length);
            //Instantiate(blow[rand], transform.position, transform.rotation);
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<MeleeAnimation>().playDieAnimation();
            //Destroy() przeniesiony do skryptu odpalanego w animacji
            //GameObject.Destroy(this.gameObject, 5.0f);
        }
    }
}