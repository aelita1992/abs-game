using UnityEngine;
using System.Collections;

public class Boomer : Enemy {
	
	public GameObject collisionOther;
	public GameObject trigger;
	public float blowDamage;
	public float blowRadius;
	public bool shouldBlow;
	public float loadTime;
	private float blowStartTime;


	public virtual void Start()
	{
		base.Start ();
		shouldBlow = false;
		if (blowRadius != 0.0f)
			trigger.GetComponent<CapsuleCollider> ().radius = blowRadius;
			
	}

	public virtual void Update()
	{
		base.Update ();
		if (shouldBlow) {
			if(blowStartTime == 0.0f) blowStartTime = Time.time;
			if(blowStartTime + loadTime < Time.time)
			{
				if(trigger.GetComponent<BoomerBlow>().isPlayerInside)
					Game.player.hit (this.blowDamage);
				this.die ();
			}

		}
	}

}