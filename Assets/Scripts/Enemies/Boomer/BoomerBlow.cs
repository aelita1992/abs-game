﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class BoomerBlow : MonoBehaviour {

	//private List<GameObject> obj;
	public bool isPlayerInside;


	void Start () {
		isPlayerInside = false;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") 
			isPlayerInside = true;
	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Player") 
			isPlayerInside = false;
	}

}
