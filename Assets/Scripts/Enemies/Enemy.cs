﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float[] fireRate;
	public GameObject[] bullet;
	public GameObject[] shotSpawn;

    public EnemyGroupController enemyGroupController;

	public GameObject[] blow;

	public int healthPts;
	public int speed;
	public int angularSpeed;
	public int acceleration;
	public float sightDistance;

	private NavMeshAgent navMeshAgent;
	public bool isAlive = true;
	private float stopTime = 0.0f;

	public virtual void Start()
	{
		navMeshAgent = GetComponent<NavMeshAgent> ();
		navMeshAgent.Stop ();
		navMeshAgent.speed = speed;
		navMeshAgent.angularSpeed = angularSpeed;
		navMeshAgent.acceleration = acceleration;

        enemyGroupController = GetComponentInParent<EnemyGroupController>();

	}

	public virtual void Update()
	{
		if (stopTime > 0.0f)
			stopTime -= Time.deltaTime;
		else
			stopTime = 0.0f;
	}

	public virtual void shoot(int i)
	{
		Instantiate (bullet[i], shotSpawn[i].transform.position, shotSpawn[i].transform.rotation);
	}

	public virtual void setStopTime(float t)
	{
		if (t > stopTime)
			stopTime = t;
	}

	public virtual float getStopTime() { return stopTime; }


	public virtual void hit(int dmg)
	{
		healthPts -= dmg;
		if (healthPts < 1)
			die ();
	}

	public virtual void die()
	{
		if (isAlive) {
			//jakies partikle tu i inne rzeczy
			isAlive = false;
            enemyGroupController.loadChildrenArray();
			int rand = Random.Range (0, blow.Length);
			Instantiate (blow [rand], transform.position, transform.rotation);
			GameObject.Destroy (this.gameObject, 0.0f);
		}
	}
}
