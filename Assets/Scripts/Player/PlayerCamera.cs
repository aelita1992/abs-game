﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
	private static PlayerCamera singleton;
	public static PlayerCamera Get()
	{
		return singleton;
	}

	private PlayerCamera(){		}

	delegate void CameraDelegate(int num);
	CameraDelegate onCameraUpdate;

	private Vector3 distance;
	public float mouseLookFactor;
	public float smoothFactor;
	//public Player player;

	void Start ()
	{
		distance = transform.position - Game.player.transform.position;
		singleton = this;
		//player = Game.player;
		//transform.position = Game.player.transform.position + distance;
		EnableStandardUpdate();
	}

	void OnDisable()
	{
		TimeManager.Get ().timeDependantUpdate -= StandardCamPositionUpdate;
	}

	public Vector3 GetDistance()
	{
		return distance;
	}

	public void EnableStandardUpdate()
	{
		TimeManager.Get ().timeDependantUpdate += StandardCamPositionUpdate;
	}

	public void DisableStandardUpdate()
	{
		TimeManager.Get ().timeDependantUpdate -= StandardCamPositionUpdate;
	}

	void StandardCamPositionUpdate () 
	{
		Vector3 targetPosition = Game.player.transform.position + distance;
		//float x = (InputMapper.pointer.x / Screen.width)  * 2 - 1;
		//float y = (InputMapper.pointer.y / Screen.height) * 2 - 1;
		targetPosition += new Vector3 (InputMapper.pointer.x, 0.0f, InputMapper.pointer.y) * mouseLookFactor;

		transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smoothFactor);
		//transform.position.x += x * mouseLookFactor;
		//transform.position.y += y * mouseLookFactor;
	}


}
