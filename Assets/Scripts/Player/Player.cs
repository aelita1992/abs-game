﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public int speed;
	
	public GameObject bulletOneShooter;
	public GameObject bulletAssault;
	public GameObject bulletSniper;
	public GameObject bulletShotgun;


	public GameObject shotRocket;
	public GameObject shotSpawn;
	public GameObject muzzleFlash;

	//private int healthPts;
	//private int shieldPts;
	//private int energyPts;

	public Dictionary <StatEnum, Vector2> stats;
	private PlayerStates state;

	private bool isAlive;

	private float shieldRechargeTimer;
	private float energyRechargeTimer;

	private Vector3 dashVelocity;
	[SerializeField]
	private float dashForce;
	[SerializeField]
	private float dashFriction;
	[SerializeField]
	private int dashEnergyCost;

	[SerializeField]
	private float flyMaxHeight;
	[SerializeField]
	private float flyForceUp;
	[SerializeField]
	private float flyForceDown;
	private float flyForce;

	private Rigidbody body;

	Rigidbody playerRigidbody;
	Vector3 savedVelocity;
	Vector3 savedAngularVelocity;

	void Awake () 
	{
		stats = new Dictionary <StatEnum, Vector2> ();
		stats.Add (StatEnum.HEALTH,	new Vector2(100, 100));
		stats.Add (StatEnum.SHIELD,	new Vector2(100, 100));
		stats.Add (StatEnum.ENERGY,	new Vector2(100, 100));
		
		state = PlayerStates.NORMAL;

		isAlive = true;
		shieldRechargeTimer = 0.0f;
		energyRechargeTimer = 0.0f;
		body = GetComponent<Rigidbody> ();

		TimeManager.Get ().timeDependantUpdate += CustomUpdate;

        //Game.setPlayer(this);
	}

	void CustomUpdate () 
	{
		if(InputMapper.inputEnabled)
		{
			//Obracanie postaci zawsze w kierunku pointera (kursora)
			Vector2 positionOnScreen = Camera.main.WorldToViewportPoint (transform.position);
			Vector2 mouse = InputMapper.pointer;
			float sw = Screen.width;
			float sh = Screen.height;
			mouse.x = mouse.x* (sw/sh);	//naprawia bug z panoramicznymi rozdzielczosciami
			mouse.x = (mouse.x + 1) * Screen.width/2;
			mouse.y = (mouse.y + 1) * Screen.height/2;
			Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(mouse);
			float angle = Mathf.Atan2 ((positionOnScreen.y - mouseOnScreen.y), 
			                           (positionOnScreen.x - mouseOnScreen.x)) * Mathf.Rad2Deg;



			//transform.rotation =  Quaternion.Euler (new Vector3(0f,-angle-90.0f,0f));

			//to samo co zakomentowana linijka wyżej, tylko wygładzone
			//rozwiązuje problem "przeskakiwania" obrotu na padzie
			Vector3 rotation = new Vector3(mouseOnScreen.x - positionOnScreen.x, 
			                               0.0f, 
			                               mouseOnScreen.y - positionOnScreen.y);
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(rotation), 20f*Time.deltaTime);
		}
		
		dashVelocity /= dashFriction;

		shieldAutoregen ();
		energyAutoregen ();

		if (transform.position.y < 0.0f)
			flyForce = 0.0f;
		
	}

	public void PauseRigidbody()
	{
		playerRigidbody = GetComponent<Rigidbody>();
		
		savedVelocity = playerRigidbody.velocity;
		savedAngularVelocity = playerRigidbody.angularVelocity;
		playerRigidbody.isKinematic = true;
	}
	
	public void ResumeRigidbody()
	{
		Rigidbody playerRigidbody = GetComponent<Rigidbody>();
		
		playerRigidbody.isKinematic = false;
		playerRigidbody.velocity = savedVelocity;
		playerRigidbody.angularVelocity = savedAngularVelocity;
		playerRigidbody.WakeUp();
	}

	public bool IsDashPossible()
	{
		if(stats[StatEnum.ENERGY].x >= dashEnergyCost)
			return true;
		
		return false;
	}

	public void changeState (PlayerStates newState)
	{
		if (state == newState)
			state = PlayerStates.NORMAL;
		else if (state == PlayerStates.NORMAL)
			state = newState;

		if (state == PlayerStates.FLY)
			flyForce = flyForceUp;
		else
			flyForce = flyForceDown;
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "Floor")
			flyForce = 0.0f;
	}

	public void dash(Vector3 dir)
	{
		if (stats[StatEnum.ENERGY].x >= dashEnergyCost) 
		{
			dashVelocity = dir.normalized * dashForce;
			ModifyStat(StatEnum.ENERGY, -dashEnergyCost);
		}
	}

	public void setVelocity(Vector3 v)
	{
		body.velocity = v;

		Vector3 vel = body.velocity;
		vel = Vector3.Scale (vel, new Vector3(speed, 1, speed));
		body.velocity = vel + dashVelocity;

		// + dashVelocity + transform.up*flyForce;
		/*if (transform.position.y > flyMaxHeight) 
		{
			Vector3 ymax = transform.position;
			ymax.y = flyMaxHeight;
			transform.position = ymax;
		}*/
	}

	public Vector3 GetVelocity()
	{
		return body.velocity;
	}

	public void pickup(int type, int quantity)
	{
		switch (type) 
		{
		case 0: //HP
			ModifyStat(StatEnum.HEALTH, quantity);
			break;

		case 1: //shield
			ModifyStat(StatEnum.SHIELD, quantity);
			break;

		case 2: //energy
			ModifyStat(StatEnum.ENERGY, quantity);
			break;

		case 3: //bullets
			WeaponManager.Get ().AddAmmo(AmmoEnum.BULLET, quantity);
			break;

		case 4: //rockets
			WeaponManager.Get ().AddAmmo(AmmoEnum.ROCKET, quantity);
			break;

		case 5: //grenades
			WeaponManager.Get ().AddAmmo(AmmoEnum.GRENADE, quantity);
			break;
		}
	}

	public void hit (float dmg) 
	{
		if(stats[StatEnum.SHIELD].x >= dmg) 
		{
			ModifyStat(StatEnum.SHIELD, -dmg);
			dmg = 0;
		}
		else if(stats[StatEnum.SHIELD].x < dmg) 
		{
			dmg -= stats[StatEnum.SHIELD].x;
			ModifyStat(StatEnum.SHIELD, -(int)stats[StatEnum.SHIELD].x);
		}

		if(stats[StatEnum.HEALTH].x > dmg) 
		{
			ModifyStat(StatEnum.HEALTH, -dmg);
		}
	}

	public void shieldRecharge()
	{
		if (stats[StatEnum.ENERGY].x > 2 && stats[StatEnum.SHIELD].x < stats[StatEnum.SHIELD].y)
		{
			ModifyStat(StatEnum.ENERGY, -2);
			ModifyStat(StatEnum.SHIELD, 1);
		}

	}

	private void shieldAutoregen()
	{
		if (shieldRechargeTimer + 1 < Time.time) 
		{
			ModifyStat(StatEnum.SHIELD, 1);
			shieldRechargeTimer = Time.time;
		}
	}
	
	private void energyAutoregen()
	{
		if (energyRechargeTimer + 1 < Time.time) 
		{
			ModifyStat(StatEnum.ENERGY, 1);
			energyRechargeTimer = Time.time;
		}

	}

	private void die()
	{
		isAlive = false;
	}

	public void ModifyStat(StatEnum statType, float quantity)
	{
		Vector2 val = stats[statType];
		val.x += quantity;
		
		Mathf.Clamp (val.x, 0, val.y);
		
		stats[statType] = val;

		HUD_Controller.Get ().GetStatBarsController().SetTargetValue(statType, Mathf.CeilToInt(stats[statType].x/10f));


		if (stats [StatEnum.HEALTH].x == 0)
			die ();
	}

	public void SetGravity(bool newValue)
	{
		body.useGravity = newValue;
	}

}
