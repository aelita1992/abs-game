﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour 
{
	private static Laser singleton;
	public static Laser Get()
	{
		return singleton;
	}

	private LineRenderer lr;
	public LayerMask laserMask;
	private float y;

	void Start () 
	{
		singleton = this;
		lr = GetComponent<LineRenderer> ();
	}

	void Update () 
	{
		lr.SetPosition (0, Game.player.shotSpawn.transform.position);
		RaycastHit hit;
		if (Physics.Raycast (Game.player.shotSpawn.transform.position, Game.player.shotSpawn.transform.forward, out hit, 20f, laserMask)) 
		{
			if (hit.collider)
				lr.SetPosition (1, hit.point);	
		}
		else 
		{
			lr.SetPosition (1, Game.player.shotSpawn.transform.position + Game.player.shotSpawn.transform.forward*20f);
		}
	}

	public void Enable()
	{
		gameObject.SetActive(true);
	}

	public void Disable()
	{
		gameObject.SetActive(false);
	}
}
