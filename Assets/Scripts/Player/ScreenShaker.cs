﻿using UnityEngine;
using System.Collections;

public class ScreenShaker : MonoBehaviour 
{
	private static ScreenShaker singleton;
	public static ScreenShaker Get()
	{
		return singleton;
	}

	public PlayerCamera playerCamera;
	public Player player;

	float shake = 0f;
	float shakeAmount = 0.1f;
	float decreaseFactor = 1.0f;
	
	void Awake()
	{
		singleton = this;
	}

	public void EnableShakes(float time)
	{
		shake = time;
		PlayerCamera.Get ().DisableStandardUpdate();

		TimeManager.Get ().timeDependantUpdate += CustomUpdate;
	}
	
	void CustomUpdate() 
	{
		if (shake > 0)
		{
			Vector3 randomChange = Random.insideUnitSphere * shakeAmount;
			playerCamera.transform.position = player.transform.position + PlayerCamera.Get ().GetDistance() + randomChange;
			shake -= Time.deltaTime * decreaseFactor;
			
		} 
		else
		{
			shake = 0.0f;
			TimeManager.Get ().timeDependantUpdate -= CustomUpdate;

			PlayerCamera.Get ().EnableStandardUpdate();
		}
	}
	
}