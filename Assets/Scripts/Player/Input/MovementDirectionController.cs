﻿using UnityEngine;
using System.Collections;

public class MovementDirectionController 
{
	public MovementDirectionController()
	{
		TimeManager.Get ().timeDependantUpdate += ControlMovementDirection;
	}

	void ControlMovementDirection()
	{
		if(!InputMapper.inputEnabled)
			return;

		if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
		{
			Vector3 velocityVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			float velToDirAngle = Vector3.Angle (PlayerAnimController.Get().transform.forward, velocityVector);

			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsWalking,	 		true);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsWalking,	 		true);

			if(velToDirAngle < 90)
			{
				Vector3 playerVel =  Game.player.GetVelocity();

				if(Mathf.Abs(playerVel.x + playerVel.z) < 5)
				{
					PlayerAnimController.Get ().SetLegsProperty	(PlayerAnimProperty.Float_PlayerSpeed, 1);
					PlayerAnimController.Get ().SetUpBodyProperty(PlayerAnimProperty.Float_PlayerSpeed, 1);
				}
				else
				{
					PlayerAnimController.Get ().SetLegsProperty	(PlayerAnimProperty.Float_PlayerSpeed, 2);
					PlayerAnimController.Get ().SetUpBodyProperty(PlayerAnimProperty.Float_PlayerSpeed, 2);
				}
			}
			
			if(velToDirAngle >= 90)
			{
				Vector3 playerVel =  Game.player.GetVelocity();

				if(Mathf.Abs(playerVel.x + playerVel.z) < 5)
				{
					PlayerAnimController.Get ().SetLegsProperty	(PlayerAnimProperty.Float_PlayerSpeed, -1);
					PlayerAnimController.Get ().SetUpBodyProperty(PlayerAnimProperty.Float_PlayerSpeed, -1);
				}
				else
				{
					PlayerAnimController.Get ().SetLegsProperty	(PlayerAnimProperty.Float_PlayerSpeed, -2);
					PlayerAnimController.Get ().SetUpBodyProperty(PlayerAnimProperty.Float_PlayerSpeed, -2);
				}
			}
		}
		else
		{
			PlayerAnimController.Get().SetLegsProperty	(PlayerAnimProperty.IsWalking,	 		false);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsWalking,	 		false);
		}
	}
}
