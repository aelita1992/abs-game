﻿using UnityEngine;
using System.Collections;

public class JetpackEvent : ButtonEvents
{
	string previousFlightDir;

	public JetpackEvent()
	{
		OnButtonPressed += JetpackControl;
	}
	
	void JetpackControl()
	{
		if (Input.GetButtonDown ("Jetpack"))
		{
			InputMapper.inputEnabled = false;
			
			if(InputMapper.posState != InputMapper.PositionState.FLYING)
			{
				Game.player.SetGravity (false);
				Game.player.changeState (PlayerStates.FLY);
				InputMapper.posState = InputMapper.PositionState.FLYING;
				
				PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.FlightEnabled,		true);
				PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.FlightEnabled,		true);
				TimeManager.Get ().timeDependantUpdate += ControlFlightDirection;
			}
			else if(InputMapper.posState == InputMapper.PositionState.FLYING)
			{
				InputMapper.posState = InputMapper.PositionState.STANDING;
				HelpTextController.Get ().TempEnable();
				Game.player.changeState (PlayerStates.NORMAL);
				
				PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.FlightEnabled,		false);
				PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.FlightEnabled,		false);
				TimeManager.Get ().timeDependantUpdate -= ControlFlightDirection;
				
				if(previousFlightDir != null)
				{
					PlayerAnimController.Get().SetLegsProperty(previousFlightDir,		false);
					PlayerAnimController.Get().SetUpBodyProperty(previousFlightDir,		false);
				}
			}
			
			Game.player.setVelocity (new Vector3(0, 0, 0));
		}
	}

	void ControlFlightDirection()
	{
		if(InputMapper.posState == InputMapper.PositionState.STANDING)
			return;

		if(HelpTextController.Get ().gameObject.activeSelf)
			HelpTextController.Get ().TempDisable();

		if(previousFlightDir != null)
		{
			PlayerAnimController.Get().SetLegsProperty(previousFlightDir,		false);
			PlayerAnimController.Get().SetUpBodyProperty(previousFlightDir,		false);
		}
		
		if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
		{
			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingIdle,			false);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingIdle,		false);
			
			Vector3 velocityVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			float velToDirAngle = Vector3.Angle (PlayerAnimController.Get().transform.forward, velocityVector);
			
			float dir = Vector3.Dot(PlayerAnimController.Get().transform.forward, velocityVector);
			
			if(velToDirAngle < 45)
			{
				PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingForward,		true);
				PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingForward,	true);
				
				previousFlightDir = PlayerAnimProperty.IsFlyingForward;
			}
			
			if(velToDirAngle >= 135)
			{
				PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingBackwards,	true);
				PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingBackwards,	true);
				
				previousFlightDir = PlayerAnimProperty.IsFlyingBackwards;
			}
			
			if(velToDirAngle >= 45 && velToDirAngle < 135)
			{
				float velToDirAngle2 = Vector3.Angle (PlayerAnimController.Get().transform.right, velocityVector);
				
				if(velToDirAngle2 > 90)
				{
					PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingLeft,		true);
					PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingLeft,	true);
					
					previousFlightDir = PlayerAnimProperty.IsFlyingLeft;
				}
				else
				{
					PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingRight,	true);
					PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingRight,	true);
					
					previousFlightDir = PlayerAnimProperty.IsFlyingRight;
				}
			}
		}
		else
		{
			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsFlyingIdle,				true);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsFlyingIdle,			true);
			
			previousFlightDir = PlayerAnimProperty.IsFlyingIdle;
		}

	}
}
