﻿using UnityEngine;
using System.Collections;

public class DashEvent : ButtonEvents
{
	public DashEvent()
	{
		OnButtonPressed += PerformDash;
	}

	void PerformDash()
	{
		Vector3 velocityVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

		if(Input.GetButtonDown ("Dash") && InputMapper.posState == InputMapper.PositionState.STANDING && velocityVector != Vector3.zero) 
		{
			Game.player.SetGravity(false);
			InputMapper.inputEnabled = false;
			Laser.Get ().Disable();
			
			Vector3 inputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			
			inputVector = MaximizeInputValues(inputVector);
			
			Game.player.dash(inputVector);	
			Game.player.setVelocity (inputVector);

			PlayerAnimController.Get().SetLegsProperty	(PlayerAnimProperty.Float_RollState,	1);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.Float_RollState,	1);

			float dashAngle = TranslateInputToDashAngle();
			float dashFromMouseAngle = Mathf.Abs (PlayerAnimController.Get().transform.eulerAngles.y - dashAngle);
			
			if(dashFromMouseAngle > 90 && dashFromMouseAngle < 270)
				dashAngle += 180;
			
			Vector3 playerRotation = PlayerAnimController.Get().transform.eulerAngles;
			playerRotation.y = dashAngle;
			PlayerAnimController.Get().transform.eulerAngles = playerRotation;
		}
	}

	float TranslateInputToDashAngle()
	{
		Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		input = MaximizeInputValues(input);
		
		float dashAngle = 180 - input.z * 90;
		
		if(input.x == 0)
		{
			dashAngle = 0;
		}
		else
		{
			if(input.x == -1)
				dashAngle = 270;
			else
				dashAngle = 90;
			
			if(input.x == input.z)
				dashAngle = 45;
			
			if(input.x == -input.z)
				dashAngle =315;
		}
		
		return dashAngle;
	}
}
