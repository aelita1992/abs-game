﻿using UnityEngine;
using System.Collections;

public class CrouchEvent : ButtonEvents 
{
	public CrouchEvent()
	{
		OnButtonPressed += CrouchEnable;
		OnButtonReleased += CrouchDisable;
	}


	void CrouchEnable()
	{
		if (Input.GetButtonDown ("Crouch") && InputMapper.posState == InputMapper.PositionState.STANDING)
		{
			HelpTextController.Get ().TempDisable();
			Game.player.changeState (PlayerStates.CROUCH);
			InputMapper.posState = InputMapper.PositionState.CROUCHING;

			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsCrouching,		true);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsCrouching,	true);
			
			Game.player.setVelocity (new Vector3(0, 0, 0));
		}
	}

	void CrouchDisable()
	{
		if (Input.GetButtonUp ("Crouch"))
		{
			if(InputMapper.posState == InputMapper.PositionState.CROUCHING)
			{
				HelpTextController.Get ().TempEnable();
				InputMapper.posState = InputMapper.PositionState.STANDING;
			}
			
			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsCrouching,		false);
			PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsCrouching,	false);
		}
	}
}
