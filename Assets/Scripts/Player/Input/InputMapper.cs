﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputMapper : MonoBehaviour
{
	private static InputMapper singleton;
	public static InputMapper Get()
	{
		return singleton;
	}

	Dictionary <string, ButtonEvents> inputEvents;

	public enum PositionState {STANDING, CROUCHING, FLYING}
	public static bool inputEnabled = true;
	public bool mouseEnabled = true;
	private bool switchable = true; //debouncing dpada
	private float runningMultipler = 1;

	public static Vector3 pointer;

	public static PositionState posState;

	void Start () 
	{
		singleton = this;
		posState = PositionState.STANDING;

		inputEvents = new Dictionary<string, ButtonEvents>();
		inputEvents.Add ("Dash", 	new DashEvent());
		inputEvents.Add ("Action", 	new ActionEvent());
		inputEvents.Add ("Crouch", 	new CrouchEvent());
		inputEvents.Add ("Jetpack", new JetpackEvent());

		new MovementDirectionController();

		TimeManager.Get ().timeDependantUpdate += CustomUpdate;
	}
	
	void CustomUpdate ()
	{
		if (!inputEnabled)
			return;

		if(posState == PositionState.STANDING)
			Game.player.SetGravity (true);

		List<string> keyList = new List<string> (inputEvents.Keys);
		for (int i = 0; i < keyList.Count; i++) {
			if (Input.GetButtonDown (keyList [i])) {
				inputEvents [keyList [i]].PerformOnButtonPressed ();
			}

			if (Input.GetButtonUp (keyList [i])) {
				inputEvents [keyList [i]].PerformOnButtonReleased ();
			}
		}

		//do szybkiego restartu levelu
		//przyda sie do testowania
		if (Input.GetKeyDown (KeyCode.F2))
			Application.LoadLevel (Application.loadedLevel);

		//przełączanie rozglądania się padem i myszą
		if (Input.GetKeyDown (KeyCode.F1))
			mouseEnabled = !mouseEnabled;

		if (mouseEnabled)
		{
			float x = (Input.mousePosition.x / Screen.width)  * 2 - 1;
			float y = (Input.mousePosition.y / Screen.height) * 2 - 1;

			pointer = new Vector3 (x, y, 0.0f);
			//Debug.Log(sw/sh);
		}
		else {
			if(Mathf.Abs(Input.GetAxis ("HorizontalLook")) > 0.05f ||
			   Mathf.Abs(Input.GetAxis ("VerticalLook"))   > 0.05f)
			{
				float x = (Input.GetAxis ("HorizontalLook") + 1) * Screen.width/2;
				float y = (Input.GetAxis ("VerticalLook") + 1) * Screen.height/2;
				pointer = new Vector3(x, y, 0.0f);
			}
			else 
			{
				float x = (Input.GetAxis ("Horizontal") + 1) * Screen.width/2;
				float y = (Input.GetAxis ("Vertical") + 1) * Screen.height/2;
				pointer = new Vector3(x, y, 0.0f);
			}
		}
		
		Vector3 velocityVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

		if(Input.GetKey (KeyCode.LeftShift) && posState == PositionState.STANDING)
		{
			runningMultipler = 2f;
		}
		else
		{
			runningMultipler = 1;
		}

		if(posState != PositionState.CROUCHING)
		{
			Vector3 currentVel = Game.player.GetVelocity();

			Game.player.setVelocity (new Vector3(runningMultipler * Input.GetAxis("Horizontal"), currentVel.y, runningMultipler * Input.GetAxis("Vertical")));
		}

		if (Input.GetButtonDown ("Weapon1")) ChangeWeapon (1);
		if (Input.GetButtonDown ("Weapon2")) ChangeWeapon (2);
		if (Input.GetButtonDown ("Weapon3")) ChangeWeapon (3);
		if (Input.GetButtonDown ("Weapon4")) ChangeWeapon (4);
		if (Input.GetButtonDown ("Weapon5")) ChangeWeapon (5);
		if (Input.GetButtonDown ("Weapon6")) ChangeWeapon (6);

		//zmiana broni na dpadzie
		if (Input.GetAxis ("SwitchWeapon") > 0.5 && switchable) 
		{
			SwitchWeapon (1);
			switchable = false;
		}
		if (Input.GetAxis ("SwitchWeapon") < -0.5 && switchable) 
		{
			SwitchWeapon (-1);
			switchable = false;
		}
		if (Input.GetAxis ("SwitchWeapon") < 0.5  && 
		    Input.GetAxis ("SwitchWeapon") > -0.5 &&
		    !switchable) 
		{
			switchable = true;
		}
		
		if(Input.GetButton ("Fire") || Input.GetAxis ("Fire")>0.1f)
		{
			WeaponManager.Get ().getCurrentWeapon ().shoot ();

			PlayerAnimController.Get().SetLegsProperty(PlayerAnimProperty.IsShooting,		 	true);
		}

		if (Input.GetButton ("Shield Recharge")) Game.player.shieldRecharge ();

		if (Input.GetButton ("Reload"))
			WeaponManager.Get ().getCurrentWeapon ().reload ();
	}

	void ChangeWeapon(int weaponIndex)
	{
		WeaponManager.Get ().changeWeapon (weaponIndex - 1);
	}

	void SwitchWeapon(int a)
	{
		WeaponManager.Get ().switchWeapon (a - 1);
	}

	public ButtonEvents GetEventsObject(string buttonCommand)
	{
		return inputEvents[buttonCommand];
	}
}
