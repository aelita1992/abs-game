﻿using UnityEngine;
using System.Collections;

public class ButtonEvents
{
	public delegate void ButtonDelegate();

	public event ButtonDelegate OnButtonPressed;
	public event ButtonDelegate OnButtonReleased;

	protected Vector3 MaximizeInputValues(Vector3 inputVector)
	{
		if(inputVector.x > 0) 	inputVector.x = 1;
		if(inputVector.x < 0)	inputVector.x = -1;
		
		if(inputVector.z > 0) 	inputVector.z = 1;
		if(inputVector.z < 0)	inputVector.z = -1;
		
		return inputVector;
	}

	public void PerformOnButtonPressed()
	{
		if(OnButtonPressed != null)
		{
			OnButtonPressed();
		}
	}

	public void PerformOnButtonReleased()
	{
		if(OnButtonReleased != null)
		{
			OnButtonReleased();
		}
	}
}
