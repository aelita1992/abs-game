﻿using UnityEngine;
using System.Collections;

public class PlayerAnimController : MonoBehaviour 
{
	private static PlayerAnimController singleton;
	public static PlayerAnimController Get()
	{
		return singleton;
	}

	public Animator upperBodyAnimator;
	public Animator legAnimator;

	void Awake()
	{
		singleton = this;
	}

	public void SetLegsProperty(string PlayerAnimProperty_name, bool newValue)
	{
		legAnimator			.SetBool(PlayerAnimProperty_name, newValue);
	}

	public void SetUpBodyProperty(string PlayerAnimProperty_name, bool newValue)
	{
		upperBodyAnimator	.SetBool(PlayerAnimProperty_name, newValue);
	}

	public void SetLegsProperty(string PlayerAnimProperty_name, float newValue)
	{
		legAnimator			.SetFloat(PlayerAnimProperty_name, newValue);
	}
	
	public void SetUpBodyProperty(string PlayerAnimProperty_name, float newValue)
	{
		upperBodyAnimator	.SetFloat(PlayerAnimProperty_name, newValue);
	}
}
