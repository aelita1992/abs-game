﻿using UnityEngine;
using System.Collections;

public class PlayerModelController : MonoBehaviour 
{
	public GameObject playerObject;

	public GameObject playerLegs;
	public GameObject upperPartPivot;

	void Update ()
	{
		transform.position = playerObject.transform.position;

		if(!InputMapper.inputEnabled)
		{
			upperPartPivot.transform.eulerAngles = playerObject.transform.eulerAngles;
			playerLegs.transform.eulerAngles = playerObject.transform.eulerAngles;
			return;
		}

		Vector3 directionVector = new Vector3(0, 0, 0);

		if (Input.GetKey(KeyCode.A))
			directionVector.x = -1;
		if (Input.GetKey(KeyCode.D))
			directionVector.x = 1;

		if (Input.GetKey(KeyCode.W))
			directionVector.z = 1;
		if (Input.GetKey(KeyCode.S))
			directionVector.z = -1;

		upperPartPivot.transform.eulerAngles = playerObject.transform.eulerAngles;


		if(directionVector.x != 0 || directionVector.z != 0)
		{
			float legsAngle = TranslateInputToDashAngle(directionVector);

			float dashFromMouseAngle = Mathf.Abs (playerObject.transform.eulerAngles.y - legsAngle);
			
			if(dashFromMouseAngle > 90 && dashFromMouseAngle < 270)
				legsAngle += 180;

			Vector3 legsRotation = playerLegs.transform.eulerAngles;
			legsRotation.y = legsAngle;
			playerLegs.transform.eulerAngles = legsRotation;
		}

		if(InputMapper.posState == InputMapper.PositionState.FLYING || InputMapper.posState == InputMapper.PositionState.CROUCHING)
		{
			playerLegs.transform.eulerAngles = playerObject.transform.eulerAngles;
		}

		if(directionVector.x == 0 && directionVector.z == 0)
		{
			playerLegs.transform.eulerAngles = playerObject.transform.eulerAngles;
		}

	}

	float TranslateInputToDashAngle(Vector3 input)
	{
		float dashAngle = 180 - input.z * 90;
		
		if(input.x == 0)
		{
			if(input.z == 1)
				dashAngle = 0;

			if(input.z == -1)
				dashAngle = 180;
		}
		else
		{
			if(input.x == -1)
				dashAngle = 270;
			else
				dashAngle = 90;

			dashAngle -= input.x * input.z * 45;
		}
		
		return dashAngle;
	}
}
