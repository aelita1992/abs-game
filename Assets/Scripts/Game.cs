﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	
	public static Player player;

	void Start () 
	{
	    player = GameObject.Find ("Player").GetComponent <Player>();
        
	}

	void Update () {
	    
	}

    public static void saveGameState()
    {
        PlayerPrefs.SetFloat("PlayerHealth", player.stats[StatEnum.HEALTH].x);
        PlayerPrefs.SetFloat("PlayerEnergy", player.stats[StatEnum.ENERGY].x);
        PlayerPrefs.SetFloat("PlayerShield", player.stats[StatEnum.SHIELD].x);

        PlayerPrefs.SetFloat("AmmoBullet", WeaponManager.Get().currAndMaxAmmo[AmmoEnum.BULLET].x);
        PlayerPrefs.SetFloat("AmmoRocket", WeaponManager.Get().currAndMaxAmmo[AmmoEnum.ROCKET].x);
        PlayerPrefs.SetFloat("AmmoGrenade", WeaponManager.Get().currAndMaxAmmo[AmmoEnum.GRENADE].x);

    }

    public static void clearGameState()
    {
        PlayerPrefs.DeleteAll();
    }

    public static void loadGameState()
    {
        player.ModifyStat(StatEnum.HEALTH,
                          -player.stats[StatEnum.HEALTH].y + PlayerPrefs.GetFloat("PlayerHP"));
        player.ModifyStat(StatEnum.ENERGY,
                          -player.stats[StatEnum.ENERGY].y + PlayerPrefs.GetFloat("PlayerEnergy"));
        player.ModifyStat(StatEnum.SHIELD,
                          -player.stats[StatEnum.SHIELD].y + PlayerPrefs.GetFloat("PlayerShield"));

        WeaponManager.Get().ReduceAmmo(AmmoEnum.BULLET,
                            WeaponManager.Get().currAndMaxAmmo[AmmoEnum.BULLET].y - PlayerPrefs.GetFloat("AmmoBullet"));
        WeaponManager.Get().ReduceAmmo(AmmoEnum.ROCKET,
                            WeaponManager.Get().currAndMaxAmmo[AmmoEnum.ROCKET].y - PlayerPrefs.GetFloat("AmmoRocket"));
        WeaponManager.Get().ReduceAmmo(AmmoEnum.GRENADE,
                            WeaponManager.Get().currAndMaxAmmo[AmmoEnum.GRENADE].y - PlayerPrefs.GetFloat("AmmoGrenade"));


    }

    public static void loadScene(string scene)
	{
		GameObject obj = (GameObject)Instantiate ((GameObject) (Resources.Load ("Loading", typeof(GameObject))),
		                                          new Vector3(), 
		                                          new Quaternion());
		obj.GetComponent<LoadingScreen>().loadScene (scene);
	}

    public static void setPlayer(Player player)
    {
        Game.player = player;
    }
}
