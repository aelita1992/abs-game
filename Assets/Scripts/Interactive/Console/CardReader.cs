using UnityEngine;
using System.Collections;

public class CardReader : MonoBehaviour 
{
	public CardEnum cardType;
	public DoorController connectedDoor;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;

		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed += PerformCardTest;
		HelpTextController.Get ().Enable();
		HelpTextController.Get ().SetText (HelpMessages.AvailableAction);
	}
	
	void OnTriggerExit(Collider other)
	{
		if(other.tag != "Player")
			return;

		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= PerformCardTest;
		HelpTextController.Get ().Disable();
	}

	void PerformCardTest()
	{
		if(CardManager.Get ().IsCardCollected(cardType))
		{
			connectedDoor.SetIsLocked(false);
			connectedDoor.Open ();
			GetComponent<Collider>().enabled = false;
		}
		else
		{
			HelpTextController.Get ().SetText ("No dollars in yo' pocket");
		}
	}
}
