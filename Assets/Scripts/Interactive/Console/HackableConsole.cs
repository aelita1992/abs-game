﻿using UnityEngine;
using System.Collections;

public class HackableConsole : MonoBehaviour
{
	public InteractionTriggerController interController;
	public Hackable hackableItem;

	bool hackingInProgress 	= false;
	float repairPercentage 	= 0;
	float repairSpeed	 	= 0.5f;

	void OnEnable()
	{
		if(interController != null)
		{
			interController.OnEnter += OnEnter;
			interController.OnExit += OnExit;
		}
	}
	
	void OnDisable()
	{
		if(interController != null)
		{
			interController.OnEnter -= OnEnter;
			interController.OnExit -= OnExit;
		}
	}
	
	private void OnEnter()
	{
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed += PerformHack;
		HelpTextController.Get ().Enable();
		HelpTextController.Get ().SetText (HelpMessages.HackingPossible);
	}
	
	private void OnExit()
	{
		TimeManager.Get ().timeDependantUpdate -= HackingInProgress;
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= PerformHack;
		ProgressBarController.Get ().gameObject.SetActive (false);
		HelpTextController.Get ().Disable();
	}
	
	void PerformHack()
	{
		if(hackingInProgress)
			return;

		InputMapper.inputEnabled = false;

		Vector3 targetPosition = this.transform.position;
		targetPosition.y = PlayerAnimController.Get ().transform.position.y;
		PlayerAnimController.Get ().transform.LookAt (targetPosition);

		PlayerAnimController.Get ().SetLegsProperty		(PlayerAnimProperty.IsUsingTerminal,	true);
		PlayerAnimController.Get ().SetUpBodyProperty	(PlayerAnimProperty.IsUsingTerminal, 	true);

		ProgressBarController.Get ().energyBar.SetValueCurrent(0);
		ProgressBarController.Get ().gameObject.SetActive (true);
		TimeManager.Get ().timeDependantUpdate += HackingInProgress;

		/*Vector3 energyBarPosition = ProgressBarController.Get ().mainCamera.ScreenToWorldPoint(transform.position);
		energyBarPosition.z = 10;
		Debug.LogError (energyBarPosition + " " + transform.position);
		ProgressBarController.Get ().energyBarTransform.localPosition = energyBarPosition;*/
		//PlayerAnimController.Get ().gameObject.transform.LookAt (this.gameObject.transform);
	}

	void HackingInProgress()
	{
		hackingInProgress = true;
		Game.player.setVelocity (new Vector3(0, 0, 0));
		repairPercentage += repairSpeed;
		ProgressBarController.Get ().energyBar.SetValueCurrent((int)repairPercentage);

		if(repairPercentage >= 100)
		{
			InputMapper.inputEnabled = true;
			HelpTextController.Get ().Disable();
			TimeManager.Get ().timeDependantUpdate -= HackingInProgress;

			PlayerAnimController.Get ().SetLegsProperty		(PlayerAnimProperty.IsUsingTerminal,	false);
			PlayerAnimController.Get ().SetUpBodyProperty	(PlayerAnimProperty.IsUsingTerminal, 	false);

			ProgressBarController.Get ().gameObject.SetActive (false);
			GetComponent<Collider>().enabled = false;

			hackableItem.SetIsLocked(false);
			hackableItem.Open();
		}
	}
}
