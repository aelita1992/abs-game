﻿using UnityEngine;
using System.Collections;

public class ChestController : Hackable 
{
	public InteractionTriggerController interController;
	private Animator chestAnimator;

	public bool isLocked = false;
	bool isLooted = false;

	void Awake()
	{
		chestAnimator = GetComponent<Animator>();
	}
	
	void OnEnable()
	{
		interController.OnEnter += OnEnter;
		interController.OnExit 	+= OnExit;
	}
	
	void OnDisable()
	{
		interController.OnEnter -= OnEnter;
		interController.OnExit 	-= OnExit;
	}
	
	private void OnEnter()
	{
		if(isLooted)
			return;

		if(!isLocked)
		{
			InputMapper.Get ().GetEventsObject("Action").OnButtonPressed += Open;
			HelpTextController.Get ().SetText (HelpMessages.AvailableAction);
		}
		else
		{
			HelpTextController.Get ().SetText (HelpMessages.ChestLocked);
		}

		HelpTextController.Get ().Enable();

	}

	private void OnExit()
	{
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= Open;
		HelpTextController.Get ().Disable();
	}

	public override void SetIsLocked(bool newValue)
	{
		isLocked = newValue;
	}

	public override void Open()
	{
		isLooted = true;

		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= Open;
		HelpTextController.Get ().Disable();

		chestAnimator.SetBool("IsOpening", true);
		int spawnedItemsCount = Random.Range (1, 3);

		for(int i = 0; i < spawnedItemsCount; i++)
		{
			GameObject pickup = PickupPool.Get ().GetRandomPickup();
			pickup.transform.position = gameObject.transform.position + new Vector3(0, 0.5f, 0);
			pickup.SetActive (true);

			pickup.GetComponent<Rigidbody>().AddForce (new Vector3(Random.Range (-50, 50), 250, -100));
		}
	}
}
