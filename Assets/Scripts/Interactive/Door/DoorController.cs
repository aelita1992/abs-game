﻿using UnityEngine;
using System.Collections;

public class DoorController : Hackable
{
	public InteractionTriggerController interController;
	private Animator doorAnimator;

	public bool isLocked = false;

	void Awake()
	{
		doorAnimator = GetComponent<Animator>();
	}

	void OnEnable()
	{
		if(interController != null)
		{
			interController.OnEnter += OnEnter;
			interController.OnExit += OnExit;
		}
	}

	void OnDisable()
	{
		if(interController != null)
		{
			interController.OnEnter -= OnEnter;
			interController.OnExit -= OnExit;
		}
	}

	private void OnEnter()
	{
		if(isLocked)
			return;

		doorAnimator.SetBool("IsOpening", true);
		doorAnimator.SetBool("IsClosing", false);
	}

	private void OnExit()
	{
		if(isLocked)
			return;

		doorAnimator.SetBool("IsClosing", true);
		doorAnimator.SetBool("IsOpening", false);
	}

	public void EndDoorOpening()
	{
		doorAnimator.SetBool("IsOpening", false);
	}

	public void EndDoorClosing()
	{
		doorAnimator.SetBool("IsClosing", false);
	}

	public override void SetIsLocked(bool newValue)
	{
		isLocked = newValue;
	}

	public override void Open()
	{
		doorAnimator.SetBool("IsOpening", true);
		doorAnimator.SetBool("IsClosing", false);
	}
}
