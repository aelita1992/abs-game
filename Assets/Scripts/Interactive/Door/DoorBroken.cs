﻿using UnityEngine;
using System.Collections;

public class DoorBroken : MonoBehaviour
{
	public InteractionTriggerController interController;
	public GameObject kickedObject;

	bool isKicked = false;
	
	void OnEnable()
	{
		interController.OnEnter += OnEnter;
		interController.OnExit += OnExit;
	}
	
	void OnDisable()
	{
		interController.OnEnter -= OnEnter;
		interController.OnExit -= OnExit;
	}
	
	private void OnEnter()
	{
		if(isKicked)
			return;
		
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed += Kick;
		HelpTextController.Get ().Enable();
		HelpTextController.Get ().SetText (HelpMessages.KickingPossible);
	}
	
	private void OnExit()
	{
		if(isKicked)
			return;
		
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= Kick;
		HelpTextController.Get ().Disable();
	}

	void Kick()
	{
		if(isKicked)
			return;

		if(InputMapper.posState != InputMapper.PositionState.STANDING)
			return;

		isKicked = true;
		Game.player.setVelocity(Vector3.zero);
		InputMapper.inputEnabled = false;

		Vector3 targetPosition = this.transform.position;
		targetPosition.y = PlayerAnimController.Get ().transform.position.y;
		PlayerAnimController.Get ().transform.LookAt (targetPosition);

		PlayerAnimController.Get ().SetLegsProperty		(PlayerAnimProperty.IsKicking,	true);
		PlayerAnimController.Get ().SetUpBodyProperty	(PlayerAnimProperty.IsKicking, 	true);

		Invoke ("DestroyKickedObj", 0.75f);
		
		/*Vector3 energyBarPosition = ProgressBarController.Get ().mainCamera.ScreenToWorldPoint(transform.position);
		energyBarPosition.z = 10;
		Debug.LogError (energyBarPosition + " " + transform.position);
		ProgressBarController.Get ().energyBarTransform.localPosition = energyBarPosition;*/
		//PlayerAnimController.Get ().gameObject.transform.LookAt (this.gameObject.transform);
	}

	void DestroyKickedObj()
	{
		InputMapper.Get ().GetEventsObject("Action").OnButtonPressed -= Kick;
		HelpTextController.Get ().Disable();
		Destroy (kickedObject);
	}
}
