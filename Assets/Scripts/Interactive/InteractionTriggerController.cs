﻿using UnityEngine;
using System.Collections;

public class InteractionTriggerController : MonoBehaviour
{
	public delegate void Interaction();
	public event Interaction OnEnter;
	public event Interaction OnExit;

	void OnEnable()
	{
		OnExit += ResetHelpText;
	}

	void OnDisable()
	{
		OnExit += ResetHelpText;
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;

		if(OnEnter != null)
			OnEnter();
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag != "Player")
			return;

		if(OnExit != null)
			OnExit();
	}

	void ResetHelpText()
	{
		HelpTextController.Get().ResetTempValue();
	}
}
