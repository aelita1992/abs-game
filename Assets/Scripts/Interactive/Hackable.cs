﻿using UnityEngine;
using System.Collections;

public abstract class Hackable : MonoBehaviour
{
	public abstract void SetIsLocked(bool newValue);
	public abstract void Open();
}
