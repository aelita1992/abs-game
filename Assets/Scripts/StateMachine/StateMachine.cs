﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour
{
	
	protected abstract class State 
	{
		protected StateMachine setup;
		
		public State()
		{
			this.setup = StateMachine.Get;
		}
		
		public abstract void Init();
		public virtual void Exit(){}
		public virtual void onExpand(){}
		public virtual void onColapse(){}
	}
	
	class MenuState : State 
	{
		public override void Init()
		{
			Debug.Log ("MenuState");
			//menuPanel.options.OnButtonPress += onOptions;
		}
		
		public override void Exit()
		{
			//menuPanel.options.OnButtonPress -= onOptions;
			//menuPanel.Colapse ();
		}

		void onOptions()
		{
			//setup.SetState(new OptionState());
		}
		
		void onExit() 
		{
			Debug.LogWarning (" Exit Game ");
		}
	}

	class GameState : State 
	{
		public override void Init ()
		{
			Debug.Log ("GameState");
		}

		public GameState( int level ) 
		{
			Debug.Log(" Wczytano poziom : "+level+" i co teraz ? ");
			
		}
		
		public override void Exit ()
		{

		}
		
		public override void onColapse ()
		{
			//GameStateController.Get.Init();
		}
		
		public override void onExpand ()
		{		
			//Debug.LogWarning("GameState: RESET GAME STATE");
			//GameStateController.Get.InitReset();
		}
	}

	public GameObject mainCamera;
	
	static StateMachine singleton;
	static public StateMachine Get { get { return singleton;}}
	
	State currentState;
	
	//public Camera menuCamera;
	
	void Awake () 
	{
		if (StateMachine.singleton == null) 
		{
			StateMachine.singleton = this;          
		} else {
			Debug.LogWarning(string.Format("Duplicated singleton: {0} on: {1}", StateMachine.singleton, this));
			Destroy(gameObject);
		}
		
	}
	
	void Start()
	{
		Init ();
	}
	
	protected void SetState(State newState) 
	{
		if (currentState != null)
			currentState.Exit();
		currentState = newState;
		newState.Init();
	}
	
	public void Init () 
	{
		Debug.Log ("New State machine alive !");
	}
	
	
	void Show ()
	{
		currentState.onExpand();
	}
	
	void Hide () 
	{
		currentState.onColapse();
	}
}
