using UnityEngine;
using System.Collections;

public class SniperRifle : Weapon 
{
	private GameObject shotBullet;

	public SniperRifle()
	{
		shotSpawn = Game.player.shotSpawn;
		shotBullet = Game.player.bulletSniper;

		weaponName = "Sniper Rifle";
		ammoType = AmmoEnum.BULLET;

		fireRate = 2.0f;
		bulletCost = 20;
		clipSize = 3*bulletCost;
		reloadTime = 1.0f;
		//clipAmmo = WeaponManager.Get ().pickAmmo (ammoType, clipSize);
		clipAmmo = clipSize;

	}


	public override void shoot()
	{
		if (Time.time > nextFire && clipAmmo >= bulletCost) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			Vector3 a = shotSpawn.transform.rotation.eulerAngles + new Vector3 (0.0f, -90, 0.0f);
			Vector3 pos = shotSpawn.transform.position + shotSpawn.transform.forward*0.3f;
			GameObject flash = (GameObject) Instantiate (Game.player.muzzleFlash, pos, Quaternion.Euler (a));
			Destroy(flash, 0.09f);
			clipAmmo -= bulletCost;
			nextFire = Time.time + this.fireRate;
		}
	}
}
