using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager
{
	private static WeaponManager singleton;
	private Weapon[] weapons = new Weapon[6];
	private Weapon currentWeapon;
	private int currentWeaponNumber;

	public Dictionary <AmmoEnum, Vector2> currAndMaxAmmo;
	
	private WeaponManager () 
	{
		weapons [0] = (Weapon) ScriptableObject.CreateInstance("OneShooter");
		weapons [1] = (Weapon) ScriptableObject.CreateInstance("AssaultRifle");
		weapons [2] = (Weapon) ScriptableObject.CreateInstance("Shotgun");
		weapons [3] = (Weapon) ScriptableObject.CreateInstance("SniperRifle");
		weapons [4] = (Weapon) ScriptableObject.CreateInstance("RocketLauncher");
		weapons [5] = (Weapon) ScriptableObject.CreateInstance("GrenadeLauncher");

		weapons[0].isUnlocked = true;

		currentWeapon = weapons [0];
		currentWeaponNumber = 0;

		currAndMaxAmmo = new Dictionary <AmmoEnum, Vector2> ();
		currAndMaxAmmo.Add (AmmoEnum.BULLET,	new Vector2(2000, 2000));
		currAndMaxAmmo.Add (AmmoEnum.ROCKET, 	new Vector2(3, 3));
		currAndMaxAmmo.Add (AmmoEnum.GRENADE, 	new Vector2(5, 5));

		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(AmmoEnum.BULLET,	2000);
		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(AmmoEnum.ROCKET, 	3);
		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(AmmoEnum.GRENADE, 	5);
	}

	public void AddAmmo(AmmoEnum ammoType, int quantity)
	{
		Vector2 ammo = currAndMaxAmmo[ammoType];
		ammo.x += quantity;

		if(ammo.y < ammo.x)
			ammo.x = ammo.y;

		currAndMaxAmmo[ammoType] = ammo;
		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(ammoType, (int)ammo.x);
	}

	public int GetAmmo(AmmoEnum ammoType)
	{
		return (int)currAndMaxAmmo[ammoType].x;
	}

	public void ReduceAmmo(AmmoEnum ammoType, float quantity)
	{
		Vector2 ammo = currAndMaxAmmo[ammoType];
		ammo.x -= quantity;
		
		if(ammo.x < 0)
			ammo.x = 0;
		
		currAndMaxAmmo[ammoType] = ammo;
		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(ammoType, (int)ammo.x);
	}

	public int pickAmmo(AmmoEnum ammoType, int quantity)
	{
		Vector2 ammo = currAndMaxAmmo[ammoType];
		int ret;
		if (ammo.x >= quantity) {
			ammo.x -= quantity;
			ret = quantity;
		} else {
			ret = (int) ammo.x;
			ammo.x = 0;
		}
		currAndMaxAmmo[ammoType] = ammo;
		HUD_Controller.Get ().ammoDisplay.SetDisplayedValue(ammoType, (int)ammo.x);
		return ret;

	}

	public Weapon getCurrentWeapon() 
	{
		return currentWeapon;
	}

	public void changeWeapon(int weaponInd)
	{
		if(!weapons[weaponInd].isUnlocked|| weaponInd == currentWeaponNumber)
			return;

		currentWeapon = weapons [weaponInd];
		currentWeaponNumber = weaponInd;
		HUD_Controller.Get ().GetWaponIconController().ActivateWeaponField(weaponInd);
		PlayerAnimController.Get().SetUpBodyProperty(PlayerAnimProperty.IsTakingWeapon,	true);
	}

	public void switchWeapon(int a)
	{
		switch (a)
		{
		case 1:
			if(currentWeaponNumber<5)
				currentWeaponNumber++;
			//Mathf.Clamp (currentWeaponNumber, 0, 5);
			currentWeapon = weapons[currentWeaponNumber];
			HUD_Controller.Get ().GetWaponIconController().ActivateWeaponField(currentWeaponNumber);
			break;

		case -1:
			if(currentWeaponNumber>0)
				currentWeaponNumber--;
			//Mathf.Clamp (currentWeaponNumber, 0, 5);
			currentWeapon = weapons[currentWeaponNumber];
			HUD_Controller.Get ().GetWaponIconController().ActivateWeaponField(currentWeaponNumber);
			break;
		}
	}

	public void UnlockWeapon(int weaponIndex)
	{
		weapons[weaponIndex].isUnlocked = true;
	}

	public static WeaponManager Get()
	{
		if(singleton == null)
		{
			singleton = new WeaponManager();
		}

		return singleton;
	}
}