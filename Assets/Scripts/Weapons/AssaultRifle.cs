﻿using UnityEngine;
using System.Collections;

public class AssaultRifle : Weapon
{
	private GameObject shotBullet;
	
	public AssaultRifle() 
	{
		shotSpawn = Game.player.shotSpawn;
		shotBullet = Game.player.bulletAssault;

		weaponName = "Assault Rifle";
		ammoType = AmmoEnum.BULLET;

		fireRate = 0.1f;
		spreadAngle = 2.0f;
		bulletCost = 1;
		clipSize = 30*bulletCost;
		reloadTime = 1.0f;
		//clipAmmo = WeaponManager.Get ().pickAmmo (ammoType, clipSize);
		clipAmmo = clipSize;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && clipAmmo >= bulletCost) 
		{
			float spread = spreadAngle + spreadAngle*Game.player.GetComponent<Rigidbody>().velocity.magnitude*0.2f;
			float angle = Random.Range (-spread, spread);
			Vector3 a = shotSpawn.transform.rotation.eulerAngles + new Vector3 (0.0f, angle, 0.0f);
			Instantiate (shotBullet, shotSpawn.transform.position, Quaternion.Euler (a));
			a += new Vector3 (0.0f, -90, 0.0f);
			Vector3 pos = shotSpawn.transform.position + shotSpawn.transform.forward*0.3f;
			GameObject flash = (GameObject) Instantiate (Game.player.muzzleFlash, pos, Quaternion.Euler (a));
			Destroy(flash, 0.09f);
			clipAmmo -= bulletCost;
			nextFire = Time.time + this.fireRate;
		}
	}


}
