using UnityEngine;
using System.Collections;

public class OneShooter : Weapon 
{
	private GameObject shotBullet;

	public OneShooter()
	{
		shotSpawn = Game.player.shotSpawn;
		shotBullet = Game.player.bulletOneShooter;

		weaponName = "OneShooter";
		ammoType = AmmoEnum.BULLET;

		fireRate = 1.0f;
		spreadAngle = 1.0f;
		bulletCost = 2;
		clipSize = 10*bulletCost;
		reloadTime = 1.0f;
		//clipAmmo = WeaponManager.Get ().pickAmmo (ammoType, clipSize);
		clipAmmo = clipSize;
	}
	

	public override void shoot()
	{
		if (Time.time > nextFire && clipAmmo >= bulletCost) {
			float angle = Random.Range (-spreadAngle, spreadAngle);
			Vector3 a = shotSpawn.transform.rotation.eulerAngles + new Vector3 (0.0f, angle, 0.0f);
			Instantiate (shotBullet, shotSpawn.transform.position, Quaternion.Euler (a));
			a += new Vector3 (0.0f, -90, 0.0f);
			Vector3 pos = shotSpawn.transform.position + shotSpawn.transform.forward*0.3f;
			GameObject flash = (GameObject) Instantiate (Game.player.muzzleFlash, pos, Quaternion.Euler (a));
			Destroy(flash, 0.09f);
			clipAmmo -= bulletCost;
			nextFire = Time.time + this.fireRate;
		}
	}


}












