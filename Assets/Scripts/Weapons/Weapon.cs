﻿using UnityEngine;
using System.Collections;

public class Weapon : ScriptableObject 
{
	public string weaponName;
	public bool isUnlocked = true;

	protected AmmoEnum ammoType;
	protected float nextFire = 0.0f;				//do sprawdzania czy mozna strzelac
	protected float fireRate;						//przerwa miedzy strzalami
	protected float spreadAngle = 0.0f;				//kąt rozrzutu (mozna uzaleznic od predkosci gracza)
	protected int bulletCost;						//koszt strzału w punktach amunicji
	protected int clipSize;							//rozmiar magazynka
	protected int clipAmmo;							//ammo w magazynku
	protected float reloadTime;
	
	protected GameObject shotSpawn;

	public virtual void shoot()
	{

	}

	public virtual void reload()
	{
		if (clipAmmo < clipSize)
			clipAmmo = WeaponManager.Get ().pickAmmo (ammoType, clipSize);
		//animacja
		//pasek postępu?
		//czas przeładowania
	}
}
