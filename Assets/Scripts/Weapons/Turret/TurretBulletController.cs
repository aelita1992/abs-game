﻿using UnityEngine;
using System.Collections;

public class TurretBulletController : MonoBehaviour 
{
	private int dmg;

	Vector3 speedMultiplier = new Vector3(0.5f, 0.5f, 0.5f);
	Vector3 movementVector;

	void Start ()
	{
		dmg = 10;
	}
	
	void OnEnable()
	{
		movementVector = Vector3.Scale (Vector3.forward, speedMultiplier);
	}


	void Update () 
	{
		transform.Translate (movementVector);
	}
	
	void OnTriggerEnter(Collider other)
	{

	}
}
