﻿using UnityEngine;
using System.Collections;

public class TurretMissile : MonoBehaviour
{
	public GameObject blow;
	private int dmg;

	Vector3 speedMultiplier = new Vector3(0.25f, 0.25f, 0.25f);
	Vector3 movementVector;

	public float safeFlightDistance;
	Vector3 randomRotationAngle;

	bool active = false;

	public ParticleSystem flame;

	void Start ()
	{
		dmg = 10;
	}

	void OnEnable()
	{
		movementVector = Vector3.Scale (Vector3.forward, speedMultiplier);
	}

	void Update () 
	{
		if(!active)
			return;

		if(safeFlightDistance > 0)
		{
			safeFlightDistance -= movementVector.z;
		}
		else
		{
			transform.eulerAngles += randomRotationAngle;
		}

		transform.Translate (movementVector);
	}

	public void ActivateMissile()
	{
		movementVector = Vector3.Scale (Vector3.forward, speedMultiplier);
		flame.enableEmission = true;
		flame.Play ();

		randomRotationAngle = new Vector3(0, Random.Range (-1f, 1f), 0);
		Invoke ("InvokedActivator", 0.15f);
	}

	void InvokedActivator()
	{
		active = true;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Enemy" || other.tag == "StaticEnviro")
		{
			Instantiate (blow, transform.position - transform.forward, transform.rotation);
			ScreenShaker.Get ().EnableShakes(0.3f);
			GameObject.Destroy (this.gameObject);
			return;
		}		
	}
}
