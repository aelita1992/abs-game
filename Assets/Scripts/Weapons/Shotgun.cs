using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shotgun : Weapon 
{
	private int bulletCount;

	private int distance;

	private GameObject shotBullet;

	public Shotgun() 
	{
		shotSpawn = Game.player.shotSpawn;
		shotBullet = Game.player.bulletShotgun;

		weaponName = "Shotgun";
		ammoType = AmmoEnum.BULLET;

		fireRate = 1.0f;
		bulletCount = 70;
		spreadAngle = 45;
		bulletCost = 20;
		clipSize = 8*bulletCost;
		reloadTime = 1.0f;
		//clipAmmo = WeaponManager.Get ().pickAmmo (ammoType, clipSize);
		clipAmmo = clipSize;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && clipAmmo >= bulletCost) {

			for(int i=0; i<bulletCount; i++)
			{
				float angle = Random.Range (-spreadAngle, spreadAngle);
				Vector3 a = shotSpawn.transform.rotation.eulerAngles + new Vector3 (0.0f, angle, 0.0f);
				Instantiate (shotBullet, shotSpawn.transform.position, Quaternion.Euler (a));
			}
			Vector3 b = shotSpawn.transform.rotation.eulerAngles + new Vector3 (0.0f, -90, 0.0f);
			Vector3 pos = shotSpawn.transform.position + shotSpawn.transform.forward*0.3f;
			GameObject flash = (GameObject) Instantiate (Game.player.muzzleFlash, pos, Quaternion.Euler (b));
			Destroy(flash, 0.09f);
			clipAmmo -= bulletCost;
			nextFire = Time.time + this.fireRate;
		}
	}

}
