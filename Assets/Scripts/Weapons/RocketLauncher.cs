using UnityEngine;
using System.Collections;

public class RocketLauncher : Weapon 
{
	private GameObject shotRocket;


	public RocketLauncher() 
	{
		weaponName = "Rocket Launcher";

		shotSpawn = Game.player.shotSpawn;
		shotRocket = Game.player.shotRocket;

        clipSize = 1;
		fireRate = 1.0f;
		bulletCost = 1;
		clipAmmo = clipSize;
	}

	public override void shoot()
	{
        Debug.Log("RLauncher try");
		if (Time.time > nextFire && clipAmmo >= bulletCost) 
		{
			Instantiate (shotRocket, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + this.fireRate;

			WeaponManager.Get ().ReduceAmmo(AmmoEnum.ROCKET, bulletCost);
            Debug.Log("RLauncher shoot");
		}
	}
	
}
