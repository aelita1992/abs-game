﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

	[SerializeField]
	private GameObject blow;
	public float speed;

	void Start ()
	{
		GetComponent<Rigidbody> ().velocity = transform.forward * speed;
	}
	
	void Update () {

	}
	
	void OnCollisionEnter(Collision other)
	{
		if(other.collider.tag == "Enemy" || other.collider.tag == "StaticEnviro")
		{
			Instantiate (blow, transform.position - transform.forward, transform.rotation);
            //tu rzuca jakieś nulle
			//ScreenShaker.Get ().EnableShakes(0.3f);
			GameObject.Destroy (this.gameObject);
			return;
		}		
	}
}
