﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed;
	public float speedRange;
	public float lifetime;
	public float lifetimeRange;
	public int damage;
	public bool hitEnemies;
	public bool hitPlayer;

	void Start () {
		GetComponent<Rigidbody> ().velocity = transform.forward * Random.Range (speed-speedRange, speed+speedRange);
		GameObject.Destroy (this.gameObject, Random.Range (lifetime-lifetimeRange, lifetime+lifetimeRange));
	}


	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.layer == 11 && hitEnemies) //ENEMY
		{
			other.collider.GetComponent<Enemy>().hit (damage);
			GameObject.Destroy (this.gameObject);
			return;
		}	

		if(other.gameObject.layer == 8 && hitPlayer) //PLAYER
		{
			other.collider.GetComponent<Player>().hit (damage);
			GameObject.Destroy (this.gameObject);
			return;
		}	

		if(other.gameObject.layer == 12 || other.gameObject.layer == 9) //STATICENVIRO || INTERACTIVE
		{
			GameObject.Destroy (this.gameObject);
			return;
		}	
	}
}
