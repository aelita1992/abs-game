using UnityEngine;
using System.Collections;
using BehaviourMachine;

[AddComponentMenu("Custom Nodes/Decorator/Cooldown")]
public class CooldownNode : DecoratorNode {

	private float nextFire;
	public FloatVar time;

	public override Status Update () {
		if (child == null) {
			return Status.Error;
		}

		if (Time.time > nextFire) 
		{
			child.OnTick ();
			switch (child.status)
			{
			case Status.Running:
				return Status.Running;
				break;

			case Status.Success:
				nextFire = Time.time + time.Value;
				return Status.Success;
				break;

			case Status.Failure:
				return Status.Failure;
				break;

			default:
				return Status.Error;
				break;
			}
		}
		return Status.Failure;
	}
}