using UnityEngine;
using System.Collections;
using BehaviourMachine;

[AddComponentMenu("Custom Nodes/Action/Dash")]
public class DashNode : ActionNode {

    //public FloatVar dashForce;
    //public FloatVar dashFriction;
    //private Vector3 dashVelocity;

    public FloatVar speed;
	public FloatVar time;
    public LayerMask layerMask;
	private float orgSpeed, orgAccel, startTime;
	private NavMeshAgent agent;
	private Vector3 target;
    private Vector3 direction;

	// Called once when the node is created
	public virtual void Awake () {}
	
	// Called when the owner (BehaviourTree or ActionState) is enabled
	public override void OnEnable () {

	}
	
	// Called when the node starts its execution
	public override void Start () {
        agent = self.GetComponent<NavMeshAgent>();
        agent.enabled = false;
        startTime = Time.time;
        direction = self.transform.forward;

        /* agent = self.GetComponent<NavMeshAgent> ();
		orgSpeed = agent.speed;
		orgAccel = agent.acceleration;

		agent.speed = speed;
		agent.acceleration = acceleration;

		startTime = Time.time;
		target = Game.player.transform.position;
		agent.SetDestination (target);

		self.GetComponent<MeleeAnimation> ().playDashAnimation ();
		//self.GetComponent<Rigidbody> ().AddForce (self.GetComponent<Enemy>().transform.forward * 200);


		//dashVelocity = dashForce.Value * self.transform.forward;
        */
    }

	// This function is called when the node is in execution
	public override Status Update () {

        //tylko do sprawdzania wpadania na �ciany
        RaycastHit hit;
        Physics.Raycast(self.transform.position, direction, out hit, 1.0f, layerMask);
        if (hit.collider != null && (hit.collider.tag == "StaticEnviro" || hit.collider.tag == "Interactive"))
            return Status.Success;


        if (startTime + time.Value < Time.time || blackboard.floatVars[0].Value < 1.0f) {
			return Status.Success;
		}
		else {
            //agent.SetDestination (target);
            //self.transform.LookAt(Game.player.transform);
            self.transform.position += direction * speed;
			return Status.Running;
		}



		/*
		if (dashVelocity.magnitude > 0.01f) {
			//moveStop ();
			self.transform.position += dashVelocity;
			dashVelocity /= dashFriction.Value;

			return Status.Running;
		} else
			return Status.Success; 
		*/
	}
	
	// Called when the node ends its execution
	public override void End () {
        //agent.speed = self.GetComponent<Melee>().speed;
        //agent.acceleration = self.GetComponent<Melee>().acceleration;
        agent.enabled = true;
		agent.ResetPath ();
        startTime = 0.0f;

		//dashVelocity = new Vector3 ();
	}
	
	// Called when the owner (BehaviourTree or ActionState) is disabled
	public override void OnDisable () {}
	
	// This function is called to reset the default values of the node
	public override void Reset () {}
	
	// Called when the script is loaded or a value is changed in the inspector (Called in the editor only)
	public override void OnValidate () {}
}