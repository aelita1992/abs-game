﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

[AddComponentMenu("Custom Nodes/Condition/isVisible")]
public class isVisibleConditionNode : ConditionNode {

	public LayerMask layerMask;
	public FloatVar distance;
	public GameObjectVar obj;

	public override Status Update () 
	{
		//layerMask = (1 << 10) | (1 << 11);
		//layerMask = ~layerMask;
		Vector3 dir = obj.Value.transform.position - self.GetComponent<Transform> ().position;
		dir.y = self.GetComponent<Transform> ().position.y;
		RaycastHit hit;
		//Debug.DrawRay(self.GetComponent<Transform> ().position, dir);
		if (Physics.Raycast (self.GetComponent<Transform> ().position, dir, out hit, distance, layerMask)
		    && hit.collider.tag == obj.Value.tag) {
			blackboard.floatVars[0].Value = hit.distance;
            self.GetComponent<MeleeAnimation>().playOpenAnimation();

			return Status.Success;
		} else
			return Status.Failure;

	}
}
