﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;


public class moveStart : ActionNode {
	
	//public Vector3Var target;
	private NavMeshAgent agent;
	
	// Called once when the node is created
	public virtual void Awake () {}
	
	// Called when the owner (BehaviourTree or ActionState) is enabled
	public override void OnEnable () {}
	
	// Called when the node starts its execution
	public override void Start () {}
	
	// This function is called when the node is in execution
	public override Status Update () {
        
        if (self.GetComponent<Enemy> ().getStopTime () < 0.1 ) {
			agent = self.GetComponent<NavMeshAgent> ();
			if(agent.enabled) 
			{
				agent.speed = self.GetComponent<Enemy>().speed;
				agent.acceleration = self.GetComponent<Enemy>().acceleration;
				agent.Resume ();
			}
			return Status.Success;
		} else
			return Status.Failure;
	}
	
	// Called when the node ends its execution
	public override void End () {}
	
	// Called when the owner (BehaviourTree or ActionState) is disabled
	public override void OnDisable () {}
	
	// This function is called to reset the default values of the node
	public override void Reset () {}
	
	// Called when the script is loaded or a value is changed in the inspector (Called in the editor only)
	public override void OnValidate () {}
}