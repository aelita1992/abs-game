using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class shootNode : ActionNode {

	public int shootType;
	//public IntVar speed;
	//public IntVar damage;
	//public GameObject bullet;
	//public Transform shotSpawn;
	//private GameObject obj;
	
	// Called once when the node is created
	public virtual void Awake () {}
	
	// Called when the owner (BehaviourTree or ActionState) is enabled
	public override void OnEnable () {
		
	}
	
	// Called when the node starts its execution
	public override void Start () {
		//bullet = self.GetComponent<Enemy> ().bullet [shootType];
		//shotSpawn = self.GetComponent<Enemy> ().shotSpawn [shootType];
		
	}
	
	// This function is called when the node is in execution
	public override Status Update () {
		//Instantiate (bullet, shotSpawn.position, shotSpawn.rotation);
		//obj = (GameObject)Instantiate (bullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
		//obj.GetComponent<ShooterBullet> ().setSpeedAndDamage (speed.Value, damage.Value);

		self.GetComponent<Enemy> ().shoot (shootType);
		return Status.Success;
	}
	
	// Called when the node ends its execution
	public override void End () {
		
	}
	
	// Called when the owner (BehaviourTree or ActionState) is disabled
	public override void OnDisable () {}
	
	// This function is called to reset the default values of the node
	public override void Reset () {}
	
	// Called when the script is loaded or a value is changed in the inspector (Called in the editor only)
	public override void OnValidate () {}
}