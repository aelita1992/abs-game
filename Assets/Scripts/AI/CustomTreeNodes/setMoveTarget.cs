using UnityEngine;
using System.Collections;
using BehaviourMachine;


[AddComponentMenu("Custom Nodes/Action/Move by mesh")]
public class setMoveTarget : ActionNode {

	//public Vector3Var target;
	private NavMeshAgent agent;
	public Vector3Var target;

	// Called once when the node is created
	public virtual void Awake () {}
	
	// Called when the owner (BehaviourTree or ActionState) is enabled
	public override void OnEnable () {}
	
	// Called when the node starts its execution
	public override void Start () {}
	
	// This function is called when the node is in execution
	public override Status Update () {
		agent = self.GetComponent<NavMeshAgent> ();
		if(agent.enabled)
			agent.SetDestination (target);

		return Status.Success;
	}
	
	// Called when the node ends its execution
	public override void End () {}
	
	// Called when the owner (BehaviourTree or ActionState) is disabled
	public override void OnDisable () {}
	
	// This function is called to reset the default values of the node
	public override void Reset () {}
	
	// Called when the script is loaded or a value is changed in the inspector (Called in the editor only)
	public override void OnValidate () {}
}