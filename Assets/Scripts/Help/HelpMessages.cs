﻿using UnityEngine;
using System.Collections;

public class HelpMessages : MonoBehaviour 
{
	public const string AvailableAction = "Press F to interact";
	public const string HackingPossible = "Press F to hack";
	public const string KickingPossible = "Press F to kick";

	public const string ChestLocked 	= "There is a magnetic lock on the chest";
}
