﻿using UnityEngine;
using System.Collections;

public class HelpTextController : MonoBehaviour
{
	private static HelpTextController singleton;
	public static HelpTextController Get()
	{
		return singleton;
	}

	public TextMesh helpTextMesh;
	bool temporarilyDisabled = false;

	void Awake()
	{
		singleton = this;
		this.gameObject.SetActive (false);
	}

	public void Enable()
	{
		this.gameObject.SetActive (true);
	}

	public void Disable()
	{
		this.gameObject.SetActive (false);
	}

	public void ResetTempValue()
	{
		this.gameObject.SetActive (false);
		temporarilyDisabled = false;
	}

	public void TempEnable()
	{
		this.gameObject.SetActive (temporarilyDisabled);
		temporarilyDisabled = false;
	}

	public void TempDisable()
	{
		temporarilyDisabled = this.gameObject.activeSelf;
		this.gameObject.SetActive (false);
	}

	public void SetText(string newMessage)
	{
		helpTextMesh.text = newMessage;
	}
}
