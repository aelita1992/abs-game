﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour 
{
	public delegate void TimeListener();	
	public event TimeListener timeDependantUpdate;
	public event TimeListener nonPausableUpdate;
	
	bool gamePaused;
	
	private static TimeManager singleton;

	private TimeManager(){		}

	public static TimeManager Get()
	{
		return singleton;
	}

	void Awake()
	{
		singleton = this;
	}
	
	void Update()
	{
		if(!gamePaused)
		{
			if(timeDependantUpdate != null)
				timeDependantUpdate();
		}

		if(nonPausableUpdate != null)
			nonPausableUpdate();
	}
	
	public void PauseGame()
	{
		gamePaused = true;
		Debug.LogWarning ("Paused");
	}
	
	public void ResumeGame()
	{	
		gamePaused = false;
		Debug.LogWarning ("Resumed");
	}
}
