﻿using UnityEngine;
using System.Collections;

public class BlowWhenDeadAnimation : StateMachineBehaviour {

    Enemy self;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	//override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //Debug.Log("UPDATE");
       // Enemy self = animator.GetComponentInParent<Enemy>();
       // Debug.Log(self);
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        self = animator.GetComponentInParent<Enemy>();
        //int rand = Random.Range(0, self.blow.Length);
        //Instantiate(self.blow[rand], self.transform.position, self.transform.rotation);
        GameObject.Destroy(self.gameObject);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
