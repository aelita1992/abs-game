﻿using UnityEngine;
using System.Collections;

public class WelderAttack : MonoBehaviour {

	private float dmg;
	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			dmg = GetComponentInParent<Melee>().damage;
			other.GetComponent<Player>().hit (dmg/60.0f);
		}
		
	}
}
