﻿using UnityEngine;
using System.Collections;
using EnergyBarToolkit;

public class ProgressBarController : MonoBehaviour 
{
	private static ProgressBarController singleton;
	public static ProgressBarController Get()
	{
		return singleton;
	}

	public Camera 				mainCamera;
	public EnergyBar 			energyBar;
	public FilledRendererUGUI	filledBarRenderer;
	public RectTransform		energyBarTransform;

	void Awake()
	{
		singleton = this;
		this.gameObject.SetActive (false);
	}

	public Vector3 GetCorrection()
	{
		Vector3 correction = new Vector3(Screen.width/2, Screen.height/2, 0);
		return correction;
	}
}
