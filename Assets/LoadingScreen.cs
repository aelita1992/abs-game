﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

	public GameObject background;
	public GameObject text;
	public GameObject bar;

	private int progress = 0;

	public void loadScene(string scene)
	{
		StartCoroutine (loadSceneCoroutine (scene));
	}

	IEnumerator loadSceneCoroutine(string scene)
	{
		background.SetActive (true);
		text.SetActive (true);
		bar.SetActive (true);

		AsyncOperation async = Application.LoadLevelAsync (scene);

		while (!async.isDone) {
			progress = (int) (async.progress * 100);
			text.GetComponent<GUIText> ().text = "//LOADING " + progress + "%";
			bar.transform.localScale = new Vector3(async.progress,
			                                       bar.transform.localScale.y,
			                                       bar.transform.localScale.z);
			yield return null;

		}
	}
}
