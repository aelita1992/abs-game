﻿using UnityEngine;
using System.Collections;

public class MeleeAnimation : MonoBehaviour {

	public Animator upper;
	public Animator lower;

	private Vector3 prevPosition;
	private float currentSpeed;

    private Quaternion prevRotation;
    private float currentRotationSpeed;

	private int speedHash;
    private int rotationSpeedHash;
    private int openHash;
	private int[] dashHash;
	private int[] hitHash;
    private int[] dieHash;

	void Start () {
		prevPosition = transform.position;
        prevRotation = transform.rotation;
		speedHash = Animator.StringToHash ("speed");
        rotationSpeedHash = Animator.StringToHash("turnSpeed");
        openHash = Animator.StringToHash("Open");
		dashHash = new int[] {Animator.StringToHash ("Dash0"),
							  Animator.StringToHash ("Dash1"),
							  Animator.StringToHash ("Dash2")};
		hitHash = new int[] {Animator.StringToHash ("Hit0"),
							 Animator.StringToHash ("Hit1"),
							 Animator.StringToHash ("Hit2")};
        dieHash = new int[] {Animator.StringToHash ("Die0"),
                             Animator.StringToHash ("Die1"),
                             Animator.StringToHash ("Die2")};
    }
	
	// Update is called once per frame
	void Update () {
		currentSpeed = Mathf.Abs (Vector3.Magnitude (transform.position - prevPosition));
		prevPosition = transform.position;

		upper.SetFloat (speedHash, currentSpeed);
		lower.SetFloat (speedHash, currentSpeed);

        currentRotationSpeed = transform.rotation.eulerAngles.y - prevRotation.eulerAngles.y;
        prevRotation = transform.rotation;

        upper.SetFloat(rotationSpeedHash, currentRotationSpeed);
        lower.SetFloat(rotationSpeedHash, currentRotationSpeed);

    }

	public void playDashAnimation()
	{
		int a = (int) Random.Range (0, dashHash.Length);
		upper.SetTrigger (dashHash [a]);
		lower.SetTrigger (dashHash [a]);
	}

	public void playHitAnimation()
	{
		int a = (int) Random.Range (0, hitHash.Length);
		upper.SetTrigger (hitHash [a]);
		lower.SetTrigger (hitHash [a]);
	}

    public void playDieAnimation()
    {
        int a = (int)Random.Range(0, dieHash.Length);
        upper.SetTrigger(dieHash[a]);
        lower.SetTrigger(dieHash[a]);
    }

    public void playOpenAnimation()
    {
        upper.SetTrigger(openHash);
        lower.SetTrigger(openHash);

    }

}
