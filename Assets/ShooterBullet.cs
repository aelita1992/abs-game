﻿using UnityEngine;
using System.Collections;

public class ShooterBullet : MonoBehaviour {

	private int damage;
	void Start () {
		damage = 5;
		GetComponent<Rigidbody> ().velocity = 30*transform.forward;
	}
	
	
	void OnCollisionEnter(Collision other)
	{
		if(other.collider.tag == "Player")
		{
			Game.player.hit (damage);
			GameObject.Destroy (this.gameObject);
			return;
		}	
		if(other.collider.tag == "StaticEnviro")
		{
			GameObject.Destroy (this.gameObject);
			return;
		}	
	}

	public void setSpeedAndDamage(int speed, int damage)
	{
		GetComponent<Rigidbody> ().velocity = speed*transform.forward;
		this.damage = damage;
	}
}
