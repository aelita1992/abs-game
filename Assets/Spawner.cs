﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public bool isRepeatable;
	public bool isTriggered;
	public GameObject[] objects;
	public int[] quantities;
	private BoxCollider spawnArea;
	private Vector3 spawnAreaSize;
	private float xmin, xmax,
			  	  zmin, zmax;
	// Use this for initialization
	void Start () {
		if (objects.Length != quantities.Length)
			Debug.LogError ("Spawner: Arrays dimmensions not equal");
		spawnArea = GetComponent<BoxCollider> ();
		spawnAreaSize = spawnArea.size;
		xmin = transform.position.x - spawnAreaSize.x / 2;
		xmax = transform.position.x + spawnAreaSize.x / 2;
		zmin = transform.position.z - spawnAreaSize.z / 2;
		zmax = transform.position.z + spawnAreaSize.z / 2;
		if(!isTriggered)
			spawn ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void spawn()
	{
		for (int i=0; i<objects.Length; i++) {
			for(int j=0; j<quantities[i]; j++) {
				Vector3 pos = new Vector3(Random.Range (xmin, xmax),
				                          0f,
				                          Random.Range (zmin, zmax));
				Instantiate (objects[i], pos, transform.rotation);

			}
		}
		if (!isRepeatable)
			Destroy (this.gameObject, 1.0f);
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "StaticEnviro")
			Debug.LogError ("Cannot spawn in walls");
	}









}
