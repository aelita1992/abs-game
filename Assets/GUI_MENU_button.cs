﻿using UnityEngine;
using System.Collections;

public class GUI_MENU_button : MonoBehaviour {

	public string sceneToLoad;
	private GUITexture tex;

	void Start () {
		tex = GetComponent<GUITexture> ();
	}

	void Update () {
		if (tex.HitTest (Input.mousePosition) && Input.GetMouseButtonDown (0)) {
            PlayerPrefs.SetFloat("PlayerHP", 20);
			Game.loadScene(sceneToLoad);
		}
	}
}
